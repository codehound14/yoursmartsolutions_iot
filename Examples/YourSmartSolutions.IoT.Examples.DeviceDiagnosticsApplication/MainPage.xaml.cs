﻿using System;
using Windows.Devices.Gpio;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using YourSmartSolutions.IoT.Devices;

namespace YourSmartSolutions.IoT.Examples.DeviceDiagnosticsApplication
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
            //InitGPIO();
        }

        private void InitGPIO()
        {
            var gpio = GpioController.GetDefault();

            //ultrasonicRangeSensor = null; // new UltrasonicRangeSensor(gpio, ULTRASONIC_RANGE_FINDER_TRIGGER_PIN, ULTRASONIC_RANGE_FINDER_ECHO_PIN);

            // Show an error if there is no GPIO controller
            if (gpio == null)
            {
                GpioStatusTop.Text = "There is no GPIO controller on this device.";
                return;
            }

            barSensorBottomPin = gpio.OpenPin(BAR_SENSOR_BOTTOM_PIN);
            barSensorTopPin = gpio.OpenPin(BAR_SENSOR_TOP_PIN);
            barSensorBottomLedPin = gpio.OpenPin(BAR_SENSOR_BOTTOM_LED_PIN);
            barSensorTopLedPin = gpio.OpenPin(BAR_SENSOR_TOP_LED_PIN);

            // Initialize LED to the OFF state by first writing a HIGH value
            // We write HIGH because the LED is wired in a active LOW configuration
            barSensorTopLedPin.Write(GpioPinValue.High);
            barSensorTopLedPin.SetDriveMode(GpioPinDriveMode.Output);
            barSensorBottomLedPin.Write(GpioPinValue.High);
            barSensorBottomLedPin.SetDriveMode(GpioPinDriveMode.Output);

            // Check if input pull-up resistors are supported
            if (barSensorTopPin.IsDriveModeSupported(GpioPinDriveMode.InputPullUp))
                barSensorTopPin.SetDriveMode(GpioPinDriveMode.InputPullUp);
            else
                barSensorTopPin.SetDriveMode(GpioPinDriveMode.Input);

            // Set a debounce timeout to filter out switch bounce noise from a button press
            barSensorTopPin.DebounceTimeout = TimeSpan.FromMilliseconds(50);

            // Register for the ValueChanged event so our buttonPin_ValueChanged 
            // function is called when the button is pressed
            barSensorTopPin.ValueChanged += BarSensorTopPin_ValueChanged;





            // Check if input pull-up resistors are supported
            if (barSensorBottomPin.IsDriveModeSupported(GpioPinDriveMode.InputPullUp))
                barSensorBottomPin.SetDriveMode(GpioPinDriveMode.InputPullUp);
            else
                barSensorBottomPin.SetDriveMode(GpioPinDriveMode.Input);

            // Set a debounce timeout to filter out switch bounce noise from a button press
            barSensorBottomPin.DebounceTimeout = TimeSpan.FromMilliseconds(50);

            // Register for the ValueChanged event so our buttonPin_ValueChanged 
            // function is called when the button is pressed
            barSensorBottomPin.ValueChanged += BarSensorBottomPin_ValueChanged;





            GpioStatusBottom.Text = "GPIO pins initialized correctly.";
            GpioStatusTop.Text = "GPIO pins initialized correctly.";

            //double distance = ultrasonicRangeSensor.GetDistance();
            //WriteToLog($"distance: {distance}");


            ////System.Diagnostics.Debug.WriteLine("hello at: " + DateTime.Now.ToString(), OutputBlock.Text);
            //OutputBlock.Text = "New Text at: " + DateTime.Now.ToString() + "\n" + OutputBlock.Text;
            //System.Threading.Tasks.Task.Delay(1000);

            //WriteToLog("New Text");
            //System.Threading.Tasks.Task.Delay(2000);

            //WriteToLog("New Text");
            //System.Threading.Tasks.Task.Delay(1000);

            //WriteToLog("New Text");
            //System.Threading.Tasks.Task.Delay(4000);

            //WriteToLog("New Text");
            //System.Threading.Tasks.Task.Delay(6000);




        }

        /// <see cref="https://social.msdn.microsoft.com/Forums/en-US/83fed65f-a37e-472e-b8f3-0a3d46f4c8fb/how-to-update-gui-elements-in-a-frame-control-from-async-task?forum=WindowsIoT"/>
        private void WriteToLog(string message)
        {
            //In a UWP app, the global Dispatcher object is a Windows.UI.Core.CoreDispatcher
            //It gives you a way to run code on the main UI thread
            Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
              () =>
              {
                  //Your UI Update Code Goes Here
                  OutputBlock.Text = message + "\n" + OutputBlock.Text;
              }
              ).AsTask();
        }

        /// <see cref="https://social.msdn.microsoft.com/Forums/en-US/83fed65f-a37e-472e-b8f3-0a3d46f4c8fb/how-to-update-gui-elements-in-a-frame-control-from-async-task?forum=WindowsIoT"/>
        private void WriteDistanceToUI(double distance)
        {
            //In a UWP app, the global Dispatcher object is a Windows.UI.Core.CoreDispatcher
            //It gives you a way to run code on the main UI thread
            Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
              () => {
                  //Your UI Update Code Goes Here
                  HelloMessage.Text = $"Distance = {distance} cm";
              }
              );
        }

        private void BarSensorBottomPin_ValueChanged(GpioPin sender, GpioPinValueChangedEventArgs gpioPinValueChangedEventArgs)
        {
            // toggle the state of the LED every time the button is pressed
            //if (gpioPinValueChangedEventArgs.Edge == GpioPinEdge.FallingEdge)
            {
                barSensorBottomLedPinValue = (barSensorBottomLedPinValue == GpioPinValue.Low) ?
                    GpioPinValue.High : GpioPinValue.Low;
                barSensorBottomLedPin.Write(barSensorBottomLedPinValue);
            }

            // need to invoke UI updates on the UI thread because this event
            // handler gets invoked on a separate thread.
            var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => {
                if (gpioPinValueChangedEventArgs.Edge == GpioPinEdge.FallingEdge)
                {
                    ledEllipseBottom.Fill = (barSensorBottomLedPinValue == GpioPinValue.Low) ?
                        bottomBrush : grayBrush;
                    GpioStatusBottom.Text = "Bottom Sensor Closed";
                }
                else
                {
                    ledEllipseBottom.Fill = (barSensorBottomLedPinValue == GpioPinValue.Low) ?
                        bottomBrush : grayBrush;
                    GpioStatusBottom.Text = "Bottom Sensor Open";
                }
            });
        }

        private double speedOfFullRevolution(TimeSpan timeSpanOfRevolution)
        {
            //double distanceOfSingleRevolution = Math.PI * Math.Pow((BAR_DIAMETER_INCHES / 2), 2);
            double distanceOfSingleRevolution = Math.PI * BAR_DIAMETER_INCHES;
            double speed = distanceOfSingleRevolution / timeSpanOfRevolution.TotalSeconds;
            return speed;
        }

        private double speedOfCloseToOpen(TimeSpan timeSpan)
        {
            double speed = BAR_DISTANCE_OF_CLOSE_TO_OPEN / timeSpan.TotalSeconds;
            return speed;
        }

        private void BarSensorTopPin_ValueChanged(GpioPin sender, GpioPinValueChangedEventArgs gpioPinValueChangedEventArgs)
        {
            DateTime timeOfCurrentEvent = DateTime.Now;

            if (gpioPinValueChangedEventArgs.Edge == GpioPinEdge.FallingEdge)
            {
                TimeSpan timeSinceLastClose = timeOfCurrentEvent - mostRecentSensorTransitionToClosed_TOP; 
                mostRecentSensorTransitionToClosed_TOP = DateTime.Now;
                //WriteToLog($"Time since last top close: {timeSinceLastClose}");
                WriteToLog($"Door Speed = : {speedOfFullRevolution(timeSinceLastClose)}");
            }
            else
            {
                TimeSpan timeBetweenOpenAndClose = timeOfCurrentEvent - mostRecentSensorTransitionToClosed_TOP;
                TimeSpan timeSinceLastOpen = timeOfCurrentEvent - mostRecentSensorTransitionToOpen_TOP;
                mostRecentSensorTransitionToOpen_TOP = DateTime.Now;
                //WriteToLog($"Time since last top open: {timeSinceLastOpen}");
                //WriteToLog($"timeBetweenOpenAndClose: {timeBetweenOpenAndClose}");
                WriteToLog($"Estimated Door Speed = : {speedOfCloseToOpen(timeBetweenOpenAndClose)}");

                //double distance = ultrasonicRangeSensor.GetDistance();
                //WriteToLog($"Estimated distance: {distance}");

                //distance = ultrasonicRangeSensor.GetDistanceOfObjectFromSensor();
                //WriteToLog($"Estimated distance (Mine): {distance}");


                double distance = -33.33d; // ultrasonicRangeSensor.GetDistanceOfObjectFromSensor();
                WriteDistanceToUI(distance);
            }

            {
                barSensorTopLedPinValue = (barSensorTopLedPinValue == GpioPinValue.Low) ?
                    GpioPinValue.High : GpioPinValue.Low;
                barSensorTopLedPin.Write(barSensorTopLedPinValue);
            }

            // need to invoke UI updates on the UI thread because this event
            // handler gets invoked on a separate thread.
            var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => {
                if (gpioPinValueChangedEventArgs.Edge == GpioPinEdge.FallingEdge)
                {
                    ledEllipseTop.Fill = (barSensorTopLedPinValue == GpioPinValue.Low) ? 
                        topBrush : grayBrush;
                    GpioStatusTop.Text = "Top Sensor Closed";
                }
                else
                {
                    ledEllipseTop.Fill = (barSensorTopLedPinValue == GpioPinValue.Low) ?
                        topBrush : grayBrush;
                    GpioStatusTop.Text = "Top Sensor Open";
                }
            });
        }



        private void ClickMe_Click(object sender, RoutedEventArgs e)
        {
            HelloMessage.Text = "Hello, Pete on Windows 10 IoT Core 2018-12-19!";
        }

        //UltrasonicRangeSensor ultrasonicRangeSensor;

        DateTime mostRecentSensorTransitionToClosed_BOTTOM = DateTime.Now;
        DateTime mostRecentSensorTransitionToOpen_BOTTOM = DateTime.Now;

        DateTime mostRecentSensorTransitionToClosed_TOP = DateTime.Now;
        DateTime mostRecentSensorTransitionToOpen_TOP = DateTime.Now;

        private const float BAR_DIAMETER_INCHES = 1.0f;
        private const float BAR_DISTANCE_OF_CLOSE_TO_OPEN = .45f;

        private const int BAR_SENSOR_BOTTOM_LED_PIN = 13;
        private const int BAR_SENSOR_TOP_LED_PIN = 6;
        private const int BAR_SENSOR_TOP_PIN = 5;
        private const int BAR_SENSOR_BOTTOM_PIN = 26;
        private const int ULTRASONIC_RANGE_FINDER_ECHO_PIN = 16;
        private const int ULTRASONIC_RANGE_FINDER_TRIGGER_PIN = 20;

        private GpioPin barSensorBottomLedPin;
        private GpioPin barSensorTopLedPin;
        private GpioPin barSensorTopPin;
        private GpioPin barSensorBottomPin;

        private GpioPinValue barSensorTopLedPinValue = GpioPinValue.High;
        private GpioPinValue barSensorBottomLedPinValue = GpioPinValue.High;

        private SolidColorBrush bottomBrush = new SolidColorBrush(Windows.UI.Colors.Blue);
        private SolidColorBrush topBrush = new SolidColorBrush(Windows.UI.Colors.Red);
        private SolidColorBrush grayBrush = new SolidColorBrush(Windows.UI.Colors.LightGray);

        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Exit();
        }
    }
}
