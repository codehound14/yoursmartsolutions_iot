﻿#### This app will allow you to test your devices and device configurations
#### Made for Raspberry Pi?
| Breadboard Diagram                                                                        | Schematic                                                                          |
| ----------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------- |
| ![Breadboard connections](../../../Resources/images/PushButton/RPi2_PushButton_bb.png)      | ![Circuit Schematic](../../../Resources/images/PushButton/RPi2_PushButton_schem.png) |

<sub>*Images made with [Fritzing](http://fritzing.org/)*</sub>


What will we need?

Raspberry Pi 2
HC-SR04 Ultrasonic Sensor
Breadboard
Female-to-Female Jumper Wires
Resistor 1kΩ
Resistor 2kΩ



What will we need?
•Raspberry Pi 2
•HC-SR04 Ultrasonic Sensor
•Breadboard
•Female-to-Female Jumper Wires
•Resistor 1kΩ
•Resistor 2kΩ

<p>The Raspberry Pi is a computer that is very cheap and quite small. It has hardware pins called GPIO pins that allow you to connect all manor of sensors, control boards, and other things. The GPIO pins can then be accessed directly by your code.</p>
<figure data-shortcode="caption" id="attachment_media-4" style="width: 804px" class="wp-caption alignnone"><img data-attachment-id="106" data-permalink="https://uwpstories.wordpress.com/2015/12/02/hc-sr04-ultrasonic-sensor-on-raspberry-pi-2/rp2_pinout/" data-orig-file="https://uwpstories.files.wordpress.com/2015/12/rp2_pinout.png?w=712" data-orig-size="804,550" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="RP2_Pinout" data-image-description="" data-medium-file="https://uwpstories.files.wordpress.com/2015/12/rp2_pinout.png?w=712?w=300" data-large-file="https://uwpstories.files.wordpress.com/2015/12/rp2_pinout.png?w=712?w=712" class="alignnone size-full wp-image-106" src="https://uwpstories.files.wordpress.com/2015/12/rp2_pinout.png?w=712" alt="RP2_Pinout.png" srcset="https://uwpstories.files.wordpress.com/2015/12/rp2_pinout.png?w=712 712w, https://uwpstories.files.wordpress.com/2015/12/rp2_pinout.png?w=150 150w, https://uwpstories.files.wordpress.com/2015/12/rp2_pinout.png?w=300 300w, https://uwpstories.files.wordpress.com/2015/12/rp2_pinout.png?w=768 768w, https://uwpstories.files.wordpress.com/2015/12/rp2_pinout.png 804w" sizes="(max-width: 712px) 100vw, 712px"   /><figcaption class="wp-caption-text">Here is a super useful image showing every pin usage on Raspberry Pi 2.</figcaption></figure>

<p>&nbsp;</p>
<p>&nbsp;</p>
<h3><b>What is an HC-SR04 Ultrasonic Sensor?</b></h3>
<p>It&#8217;s a cheap sensor that can be used to measure the distance between itself and an object in front of it by sending an ultrasonic pulse and listening for its echo. The HC-SR04 can be connected to many things including the Raspberry Pi.</p>
<p><img data-attachment-id="127" data-permalink="https://uwpstories.wordpress.com/2015/12/02/hc-sr04-ultrasonic-sensor-on-raspberry-pi-2/hc-sr04/" data-orig-file="https://uwpstories.files.wordpress.com/2015/12/hc-sr04.jpg" data-orig-size="1023,845" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;3.5&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;HX-DC1&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;1403889049&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;6.8&quot;,&quot;iso&quot;:&quot;50&quot;,&quot;shutter_speed&quot;:&quot;0.011904761904762&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;1&quot;}" data-image-title="hc-sr04" data-image-description="" data-medium-file="https://uwpstories.files.wordpress.com/2015/12/hc-sr04.jpg?w=300" data-large-file="https://uwpstories.files.wordpress.com/2015/12/hc-sr04.jpg?w=712" class="alignnone size-full wp-image-127" src="https://uwpstories.files.wordpress.com/2015/12/hc-sr04.jpg?w=295" alt="hc-sr04.jpg" width="295" height="244" srcset="https://uwpstories.files.wordpress.com/2015/12/hc-sr04.jpg?w=295 295w, https://uwpstories.files.wordpress.com/2015/12/hc-sr04.jpg?w=590 590w, https://uwpstories.files.wordpress.com/2015/12/hc-sr04.jpg?w=150 150w, https://uwpstories.files.wordpress.com/2015/12/hc-sr04.jpg?w=300 300w" sizes="(max-width: 295px) 100vw, 295px" /></p>
<p>&nbsp;</p>
<h3>Why do I need the resistors?</h3>
<p>The sensor&#8217;s output signal (ECHO) is rated  at 5V. However, the input pins on the Raspberry Pi GPIO are rated at 3.3V.<br />
Sending a 5V signal into that unprotected 3.3V input pin could damage the GPIO pin of the Raspberry Pi 2, which is something we want to avoid! We’ll need to use a small voltage divider circuit, consisting of two resistors, to lower the sensor output voltage to something our Raspberry Pi can handle.</p>
<p>A voltage divider consists of two resistors (R1 and R2)<img data-attachment-id="91" data-permalink="https://uwpstories.wordpress.com/2015/12/02/hc-sr04-ultrasonic-sensor-on-raspberry-pi-2/hc-sr04-tut-1/" data-orig-file="https://uwpstories.files.wordpress.com/2015/12/hc-sr04-tut-1.png" data-orig-size="800,800" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="hc-sr04-tut-1" data-image-description="" data-medium-file="https://uwpstories.files.wordpress.com/2015/12/hc-sr04-tut-1.png?w=300" data-large-file="https://uwpstories.files.wordpress.com/2015/12/hc-sr04-tut-1.png?w=712" class="  wp-image-91 alignright" src="https://uwpstories.files.wordpress.com/2015/12/hc-sr04-tut-1.png?w=340&#038;h=340" alt="hc-sr04-tut-1" width="340" height="340" srcset="https://uwpstories.files.wordpress.com/2015/12/hc-sr04-tut-1.png?w=340&amp;h=340 340w, https://uwpstories.files.wordpress.com/2015/12/hc-sr04-tut-1.png?w=680&amp;h=680 680w, https://uwpstories.files.wordpress.com/2015/12/hc-sr04-tut-1.png?w=150&amp;h=150 150w, https://uwpstories.files.wordpress.com/2015/12/hc-sr04-tut-1.png?w=300&amp;h=300 300w" sizes="(max-width: 340px) 100vw, 340px" /> in series connected to an input voltage (Vin), which needs to be reduced to our output voltage (Vout). In our circuit, Vin will be ECHO, which needs to be decreased from 5V to our Vout of 3.3V.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<ul>
<li>
<h2>Hardware part</h2>
</li>
</ul>
<div></div>
<p>There are four pins on the HC-SR04 sensor. The pin labelled VCC requires connecting to a 5V pin, the pin labelled &#8220;Gnd&#8221; requires connecting to a ground pin, and the pins &#8220;Trig&#8221; and &#8220;Echo&#8221; need to be each wired to a unique GPIO pin on the Raspberry Pi.</p>
<p>I am going to use the following pins:</p>
<p>VCC -&gt; 2nd pin<br />
TRIG-&gt; 12th pin<br />
ECHO-&gt; 16th pin<br />
GND -&gt; 6th pin</p>
<figure data-shortcode="caption" id="attachment_144" style="width: 2208px" class="wp-caption alignnone"><img data-attachment-id="144" data-permalink="https://uwpstories.wordpress.com/2015/12/02/hc-sr04-ultrasonic-sensor-on-raspberry-pi-2/rpi2hcsr04_bb-2/" data-orig-file="https://uwpstories.files.wordpress.com/2015/12/rpi2hcsr04_bb1.png?w=712" data-orig-size="2208,1383" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="rpi2hcsr04_bb" data-image-description="" data-medium-file="https://uwpstories.files.wordpress.com/2015/12/rpi2hcsr04_bb1.png?w=712?w=300" data-large-file="https://uwpstories.files.wordpress.com/2015/12/rpi2hcsr04_bb1.png?w=712?w=712" class="alignnone size-full wp-image-144" src="https://uwpstories.files.wordpress.com/2015/12/rpi2hcsr04_bb1.png?w=712" alt="rpi2hcsr04_bb.png" srcset="https://uwpstories.files.wordpress.com/2015/12/rpi2hcsr04_bb1.png?w=712 712w, https://uwpstories.files.wordpress.com/2015/12/rpi2hcsr04_bb1.png?w=1424 1424w, https://uwpstories.files.wordpress.com/2015/12/rpi2hcsr04_bb1.png?w=150 150w, https://uwpstories.files.wordpress.com/2015/12/rpi2hcsr04_bb1.png?w=300 300w, https://uwpstories.files.wordpress.com/2015/12/rpi2hcsr04_bb1.png?w=768 768w, https://uwpstories.files.wordpress.com/2015/12/rpi2hcsr04_bb1.png?w=1024 1024w" sizes="(max-width: 712px) 100vw, 712px"   /><figcaption class="wp-caption-text">Here is a simple demonstration on how to wire up your Raspberry Pi 2 with HC-Sr04 sensor.</figcaption></figure>
<p>&nbsp;</p>
<p>I think it is pretty straight-forward (and I am not a hardware guy 🙂 ) so I am gonna leave it as it is, hope you don&#8217;t mind.</p>
<p>&nbsp;</p>
<ul>
<li>
