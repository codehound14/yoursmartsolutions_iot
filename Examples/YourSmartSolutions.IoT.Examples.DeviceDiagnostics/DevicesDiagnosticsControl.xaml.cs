﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YourSmartSolutions.IoT.Devices;
using YourSmartSolutions.IoT.Devices.Sensors.UltrasonicRangeSensors;
using YourSmartSolutions.IoT.DeviceUIControls;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace YourSmartSolutions.IoT.Examples.DeviceDiagnostics
{
    public sealed partial class DevicesDiagnosticsControl : UserControl
    {
        public DevicesDiagnosticsControl()
        {
            this.InitializeComponent();

            //// add first range finder
#pragma warning disable IDE0017 // Simplify object initialization
            UltrasonicRangeSensorPinConnections ultrasonicRangeSensorPinConnections = new UltrasonicRangeSensorPinConnections();
#pragma warning restore IDE0017 // Simplify object initialization
            ultrasonicRangeSensorPinConnections.Trigger = 20;
            ultrasonicRangeSensorPinConnections.Echo = 16;
            UltrasonicRangeSensorDiagnosticViewer ultrasonicRangeSensorDiagnosticViewer =
                new UltrasonicRangeSensorDiagnosticViewer(ultrasonicRangeSensorPinConnections);
            ultrasonicRangeSensorDiagnosticViewer.Name = "FirstUltrasonicRangeSensorDiagnosticViewer";
            ultrasonicRangeSensorDiagnosticViewer.DeviceInstanceNickname = "Range with no board";
            StackPanelRangeFinders.Children.Add(ultrasonicRangeSensorDiagnosticViewer);

            // add second range finder
            UltrasonicRangeSensorPinConnections ultrasonicRangeSensorPinConnections2 = new UltrasonicRangeSensorPinConnections();
            ultrasonicRangeSensorPinConnections2.Trigger = 12;
            ultrasonicRangeSensorPinConnections2.Echo = 21;
            UltrasonicRangeSensorDiagnosticViewer ultrasonicRangeSensorDiagnosticViewer2 =
                new UltrasonicRangeSensorDiagnosticViewer(ultrasonicRangeSensorPinConnections2);
            ultrasonicRangeSensorDiagnosticViewer2.Name = "SecondUltrasonicRangeSensorDiagnosticViewer";
            ultrasonicRangeSensorDiagnosticViewer2.DeviceInstanceNickname = "Range with proto board";
            StackPanelRangeFinders.Children.Add(ultrasonicRangeSensorDiagnosticViewer2);


            //
            // Add Switches
            //

            // add first switch
            SwitchBinaryDiagnosticViewer switchBinaryDiagnosticViewer = new SwitchBinaryDiagnosticViewer(gpioPinForSwitch: 5);
            switchBinaryDiagnosticViewer.DeviceInstanceNickname = "Simulated Garage Door contact";
            StackPanelBinarySwitches.Children.Add(switchBinaryDiagnosticViewer);

            // add second switch
            SwitchBinaryDiagnosticViewer switchBinaryDiagnosticViewer2 = new SwitchBinaryDiagnosticViewer(gpioPinForSwitch: 26);
            switchBinaryDiagnosticViewer2.DeviceInstanceNickname = "Sump Pump - Main Pump Top";
            StackPanelBinarySwitches.Children.Add(switchBinaryDiagnosticViewer2);


            //
            // Add Weather Sensors
            //

            WeatherSensorDiagnosticViewer weatherSensorDiagnosticViewer = new WeatherSensorDiagnosticViewer();
            weatherSensorDiagnosticViewer.DeviceInstanceNickname = "Deep Freezer";
            StackPanelWeatherSensors.Children.Add(weatherSensorDiagnosticViewer);

            WeatherSensorDiagnosticViewer_MCP9808 weatherSensorDiagnosticViewer_MCP9808 = new WeatherSensorDiagnosticViewer_MCP9808();
            weatherSensorDiagnosticViewer_MCP9808.DeviceInstanceNickname = "Garage Refrigerator";
            StackPanelWeatherSensors.Children.Add(weatherSensorDiagnosticViewer_MCP9808);

            //
            // Add I2C devices
            //
            I2CStatusDiagnosticViewer i2CStatusDiagnosticViewer = new I2CStatusDiagnosticViewer();
            StackPanelI2CBus.Children.Add(i2CStatusDiagnosticViewer);

        }
    }
}
