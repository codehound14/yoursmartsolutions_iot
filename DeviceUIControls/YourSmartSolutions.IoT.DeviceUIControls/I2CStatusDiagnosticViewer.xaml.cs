﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace YourSmartSolutions.IoT.DeviceUIControls
{
    public sealed partial class I2CStatusDiagnosticViewer : UserControl
    {
        YourSmartSolutions.IoT.Devices.I2CBus i2CBus = new Devices.I2CBus();




        #region Continous Update of UI on its own thread with its local copy of the latest sensor readings

        DispatcherTimer timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(ContinuousUpdateFrequencyMilliseconds) };

        static int ContinuousUpdateFrequencyMilliseconds { get; set; } = 700;

        private void Timer_Tick(object sender, object e)
        {
            i2CBus.GetReadingFromSensorAndRaiseEventWithResultsAsync();
            textBlockCompositStatus.Text = i2CBus.CompositStatusString;
        }
        #endregion
        public I2CStatusDiagnosticViewer()
        {
            this.InitializeComponent();
            textBlockCompositStatus.Text = i2CBus.CompositStatusString;

            i2CBus.NewReadingEvent += I2CBus_NewReadingEvent;


            // start our time that updates on our UI thread
            timer.Tick += Timer_Tick;
            //timer.Start();

            i2CBus.GetReadingFromSensorAndRaiseEventWithResultsAsync();
            //textBlockCompositStatus.Text = i2CBus.CompositStatusString;
        }

        private void I2CBus_NewReadingEvent(object sender, Devices.I2CBus.NewReadingEventEventArgs EventArgsParam)
        {
            YourSmartSolutions.IoT.Devices.I2CBus sendingSensor = sender as Devices.I2CBus;
            if (sendingSensor != null)
            {
                YourSmartSolutions.IoT.Devices.I2CBus senderI2CBus = sender as YourSmartSolutions.IoT.Devices.I2CBus;
                textBlockCompositStatus.Text = $"{senderI2CBus.CompositStatusString}";
                textNumberOfDevicesFound.Text = EventArgsParam.NumberOfI2CDevices.ToString("X2");
                textBlockGeneralDiagnostics.Text = $"reading at {EventArgsParam.TimeOfReading}";
            }
            else
            {
                throw new System.NotImplementedException("Unrecognized sender type cased exception in I2CBus_NewReadingEvent");
            }
        }

        private void ButtonScanI2CBus_Click(object sender, RoutedEventArgs e)
        {
            i2CBus.GetReadingFromSensorAndRaiseEventWithResultsAsync();
            //textBlockCompositStatus.Text = i2CBus.CompositStatusString;
        }
    }
}
