﻿using System;
using System.Threading;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using YourSmartSolutions.IoT.Devices.Sensors.UltrasonicRangeSensors;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace YourSmartSolutions.IoT.DeviceUIControls
{
    /// <summary>
    /// Excercise range finder and show diagnostic data
    /// </summary>
    /// <see cref="https://www.c-sharpcorner.com/article/show-user-friendly-enum-value-into-the-combobox/"/>
    public sealed partial class UltrasonicRangeSensorDiagnosticViewer : UserControl
    {
        public YourSmartSolutions.IoT.Devices.Sensors.UltrasonicRangeSensors.HC_SR04.HC_SR04 ultrasonicRangeSensor { get; set; } = null;
        private const int ULTRASONIC_RANGE_FINDER_ECHO_PIN = 16;
        private const int ULTRASONIC_RANGE_FINDER_TRIGGER_PIN = 20;

        public UltrasonicRangeSensorPinConnections ultrasonicRangeSensorPinConnections { get; set; } = new UltrasonicRangeSensorPinConnections();

        #region Continous Update of UI on its own thread with its local copy of the latest sensor readings

        DispatcherTimer timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(ContinuousUpdateFrequencyMilliseconds) };

        static int ContinuousUpdateFrequencyMilliseconds { get; set; } = 700;

        double mostRecentAverageDistanceReadingInCM = -1;
        double mostRecentDistanceReadingInCM = -1;

        private void Timer_Tick(object sender, object e)
        {
            textBlockCurrentDistanceReading.Text = $"Continuous " + formatDistanceReadingToDiagnosticText(mostRecentDistanceReadingInCM);
            textBlockAverageDistanceReading.Text = $"   Average " + formatDistanceReadingToDiagnosticText(mostRecentAverageDistanceReadingInCM);
        }
        #endregion


        public UltrasonicRangeSensorDiagnosticViewer(UltrasonicRangeSensorPinConnections ultrasonicRangeSensorPinConnections) : this()
        {
            this.ultrasonicRangeSensorPinConnections = ultrasonicRangeSensorPinConnections;

            // set UI to match pin connections from constructor
            comboBoxTriggerPin.SelectedValue = comboBoxTriggerPin.FindName(
                $"Trigger_{ultrasonicRangeSensorPinConnections.Trigger.ToString()}");

            comboBoxEchoPin.SelectedValue = comboBoxEchoPin.FindName(
                $"Echo_{ultrasonicRangeSensorPinConnections.Echo.ToString()}");



            InitializeRangeFinder();


            //startSendingRangeData(ContinuousUpdateFrequencyMilliseconds);

            //this.timer.Tick += new EventHandler<object> new EventHandler(timer_Tick);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        public UltrasonicRangeSensorDiagnosticViewer()
        {
            this.InitializeComponent();
        }

        public string DeviceInstanceNickname
        {
            get { return ultrasonicRangeSensor.DeviceInstanceNickname; }
            set
            {
                if (ultrasonicRangeSensor != null)
                {
                    ultrasonicRangeSensor.DeviceInstanceNickname = value;
                    textDeviceName.Text = ultrasonicRangeSensor.DeviceInstanceNickname;
                }
            }
        }

        private void CheckBoxTriggerWithTimer_Checked(object sender, RoutedEventArgs e)
        {
            if (ultrasonicRangeSensor != null)
            {
                ultrasonicRangeSensor.SendSensorDataContinuallyBasedOntimer = checkBoxTriggerWithTimer.IsChecked.GetValueOrDefault(false);
            }
        }

        private void CheckBoxTriggerWithTimer_Unchecked(object sender, RoutedEventArgs e)
        {
            if (ultrasonicRangeSensor != null)
            {
                ultrasonicRangeSensor.SendSensorDataContinuallyBasedOntimer = checkBoxTriggerWithTimer.IsChecked.GetValueOrDefault(false);
            }
        }

        private void InitializeRangeFinder()
        {
            if (ultrasonicRangeSensor == null)
            {
                try
                {
                    ultrasonicRangeSensor = new Devices.Sensors.UltrasonicRangeSensors.HC_SR04.HC_SR04(ultrasonicRangeSensorPinConnections);
                }
                catch (Exception exception)
                {
                    string errorMessage = $"We have a null range finder due to exception '{exception.Message}'";
                    textBlockCurrentDistanceReading.Text = errorMessage;
                    return;
                }

                if (ultrasonicRangeSensor == null)
                {
                    textBlockCurrentDistanceReading.Text = $"We have a null range finder";
                    return;
                }

                ultrasonicRangeSensor.ContinuousUpdateFrequencyMilliseconds = UltrasonicRangeSensorDiagnosticViewer.ContinuousUpdateFrequencyMilliseconds;

                checkBoxTriggerWithTimer.IsChecked = ultrasonicRangeSensor.SendSensorDataContinuallyBasedOntimer;

                ultrasonicRangeSensor.NewSensorReadingEvent += UltrasonicRangeSensor_NewSensorReadingEvent;

                //textBlockGeneralDiagnostics.Text = ultrasonicRangeSensor.DevicePartNumberIdentifier;

                textPartNumber.Text = ultrasonicRangeSensor.DevicePartNumberIdentifier;
                textDeviceName.Text = ultrasonicRangeSensor.DeviceInstanceNickname;
            }
        }

        private void ButtonGetReading_Click(object sender, RoutedEventArgs e)
        {

            InitializeRangeFinder();

            if (ultrasonicRangeSensor != null)
            {
                double distanceOfObjectFromSensor = ultrasonicRangeSensor.GetDistanceOfObjectFromSensor();
                textBlockCurrentDistanceReading.Text = formatDistanceReadingToDiagnosticText(distanceOfObjectFromSensor);
            }
        }

        private void UltrasonicRangeSensor_NewSensorReadingEvent(object sender, UltrasonicRangeSensor.NewSensorReadingEventEventArgs EventArgsParam)
        {
            UltrasonicRangeSensor sendingSensor = sender as UltrasonicRangeSensor;
            if(sendingSensor != null)
            {
                mostRecentDistanceReadingInCM = EventArgsParam.DistanceReadingInCM;
                mostRecentAverageDistanceReadingInCM = EventArgsParam.DistanceReadingAverageInCM;
            }
            else
            {
                throw new System.NotImplementedException("Unrecognized sender type cased exception in UltrasonicRangeSensor_NewSensorReadingEvent");
            }
        }

        private string formatDistanceReadingToDiagnosticText(double distanceOfObjectFromSensor)
        {
            return $"Current Distance Reading: {distanceOfObjectFromSensor.ToString("#00.000")} cm";
            //return $"Current Distance Reading: {distanceOfObjectFromSensor.ToString("0.##")} cm";
        }

        private void ComboBoxTriggerPin_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem selecteItem = comboBoxTriggerPin.SelectedItem as ComboBoxItem;

            string selectedPinValueAsTest = selecteItem.Content.ToString();

        }

        private void ComboBoxEchoPin_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
