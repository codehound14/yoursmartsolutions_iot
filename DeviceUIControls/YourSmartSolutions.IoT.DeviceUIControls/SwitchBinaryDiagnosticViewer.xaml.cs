﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Gpio;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace YourSmartSolutions.IoT.DeviceUIControls
{
    public sealed partial class SwitchBinaryDiagnosticViewer : UserControl
    {
        Microsoft.IoT.Devices.Input.Switch inputSwitch = new Microsoft.IoT.Devices.Input.Switch();
        Devices.Sensors.SwitchBinary switchBinary = new Devices.Sensors.SwitchBinary();
        public SwitchBinaryDiagnosticViewer(int gpioPinForSwitch)
        {
            this.InitializeComponent();


            //inputSwitch.UsePullResistors = false;

            var gpio = GpioController.GetDefault();

            try
            {
                Windows.Devices.Gpio.GpioPin gpioPin = gpio.OpenPin(gpioPinForSwitch);
                inputSwitch.Pin = gpioPin;


                inputSwitch.OnValue = GpioPinValue.Low;

                inputSwitch.Switched += InputSwitch_Switched;

                //textPartNumber.Text = inputSwitch.DevicePartNumberIdentifier;
                //textDeviceName.Text = inputSwitch.DeviceInstanceNickname;
            }
            catch (Exception exception)
            {
                //In a UWP app, the global Dispatcher object is a Windows.UI.Core.CoreDispatcher
                //It gives you a way to run code on the main UI thread
                var updater = Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {
                    //Your UI Update Code Goes Here
                    textBlockGeneralDiagnostics.Text = $"Binary Switch Failed to initialize due to exception = {exception.Message}";
                    textBlockGeneralDiagnostics.Visibility = Visibility.Visible;
                }
                  );
            }
        }

        public string DeviceInstanceNickname
        {
            get { return switchBinary.DeviceInstanceNickname; }
            set
            {
                if (inputSwitch != null)
                {
                    switchBinary.DeviceInstanceNickname = value;
                    textDeviceName.Text = switchBinary.DeviceInstanceNickname;
                }
            }
        }

        private void InputSwitch_Switched(Microsoft.IoT.DeviceCore.Input.ISwitch sender, bool args)
        {

            //In a UWP app, the global Dispatcher object is a Windows.UI.Core.CoreDispatcher
            //It gives you a way to run code on the main UI thread
            var updater =  Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {
                //Your UI Update Code Goes Here
                textBlockGeneralDiagnostics.Text = $"Binary Switch State = {inputSwitch.IsOn}";
                textBlockGeneralDiagnostics.Visibility = Visibility.Visible;
            }
              );


            // need to invoke UI updates on the UI thread because this event
            // handler gets invoked on a separate thread.
            var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => {

                sensorStateToggleSwitch.IsOn = inputSwitch.IsOn;

            });
        }
    }
}
