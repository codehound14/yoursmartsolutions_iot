﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YSS.IoT.Devices.Sensors.Weather;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace YourSmartSolutions.IoT.DeviceUIControls
{
    public sealed partial class WeatherSensorDiagnosticViewer_MCP9808 : UserControl
    {
        YSS.IoT.Devices.Sensors.Weather.MCP9808 sensor = new YSS.IoT.Devices.Sensors.Weather.MCP9808();




        #region Continous Update of UI on its own thread with its local copy of the latest sensor readings

        DispatcherTimer timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(ContinuousUpdateFrequencyMilliseconds) };

        static int ContinuousUpdateFrequencyMilliseconds { get; set; } = 700;



        private double m_MostRecentTemperatureReading;
        public double MostRecentTemperatureReading
        {
            get
            {
                return m_MostRecentTemperatureReading;
            }
            set
            {
                m_MostRecentTemperatureReading = value;
                //System.Diagnostics.Debug.WriteLine($"In WeatherSensorDiagnosticViewer_MCP9808, Most recent Temperature Reading {m_MostRecentTemperatureReading}");
            }
        }


        private double m_MostRecentHumidityReading;
        public double MostRecentHumidityReading
        {
            get
            {
                return m_MostRecentHumidityReading;
            }
            set
            {
                m_MostRecentHumidityReading = value;
                //System.Diagnostics.Debug.WriteLine($"In WeatherSensorDiagnosticViewer_MCP9808, Most recent Humidity Reading {m_MostRecentHumidityReading}");
            }
        }

        private void Timer_Tick(object sender, object e)
        {
            textBlockCurrentTemperatureReading.Text = $"Temperature " + MostRecentTemperatureReading.ToString();
            textBlockCurrentHumidityReading.Text    = $"   Humidity " + MostRecentHumidityReading.ToString();
        }
        #endregion


        public WeatherSensorDiagnosticViewer_MCP9808()
        {
            this.InitializeComponent();
            textPartNumber.Text = sensor.DevicePartNumberIdentifier;
            textDeviceName.Text = sensor.DeviceInstanceNickname;

            sensor.NewSensorReadingEvent += NewSensorReadingEvent;


            // start our time that updates on our UI thread
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void NewSensorReadingEvent(object sender, YSS.IoT.Devices.Sensors.Weather.MCP9808.NewSensorReadingEventEventArgs EventArgsParam)
        {
            MCP9808 sendingSensor = sender as MCP9808;
            if (sendingSensor != null)
            {
                MostRecentTemperatureReading = EventArgsParam.TemperatureReadingInCelcius;
                MostRecentHumidityReading = EventArgsParam.HumidityReading;
            }
            else
            {
                throw new System.NotImplementedException("Unrecognized sender type cased exception in MCP9808 NewSensorReadingEvent");
            }

        }

        public string DeviceInstanceNickname
        {
            get { return sensor.DeviceInstanceNickname; }
            set
            {
                sensor.DeviceInstanceNickname = value;
                textDeviceName.Text = sensor.DeviceInstanceNickname;
            }
        }

        private void CheckBoxTriggerWithTimer_Checked(object sender, RoutedEventArgs e)
        {
            if (sensor != null)
            {
                sensor.SendSensorDataContinuallyBasedOntimer = checkBoxTriggerWithTimer.IsChecked.GetValueOrDefault(false);
            }
        }

        private void CheckBoxTriggerWithTimer_Unchecked(object sender, RoutedEventArgs e)
        {
            if (sensor != null)
            {
                sensor.SendSensorDataContinuallyBasedOntimer = checkBoxTriggerWithTimer.IsChecked.GetValueOrDefault(false);
            }
        }
    }
}
