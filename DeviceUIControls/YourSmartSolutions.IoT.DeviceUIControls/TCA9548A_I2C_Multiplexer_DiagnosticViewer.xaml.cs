﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YSS.IoT.Devices.Sensors.Weather.HTU21D;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace YourSmartSolutions.IoT.DeviceUIControls
{
    public sealed partial class TCA9548A_I2C_Multiplexer_DiagnosticViewer : UserControl
    {
        YSS.IoT.Devices.Sensors.Weather.HTU21D.HTU21D htu21d = new YSS.IoT.Devices.Sensors.Weather.HTU21D.HTU21D();




        #region Continous Update of UI on its own thread with its local copy of the latest sensor readings

        DispatcherTimer timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(ContinuousUpdateFrequencyMilliseconds) };

        static int ContinuousUpdateFrequencyMilliseconds { get; set; } = 700;



        private double m_MostRecentTemperatureReading;
        public double MostRecentTemperatureReading
        {
            get
            {
                return m_MostRecentTemperatureReading;
            }
            set
            {
                m_MostRecentTemperatureReading = value;
                //System.Diagnostics.Debug.WriteLine($"In TCA9548A_I2C_Multiplexer_DiagnosticViewer, Most recent Temperature Reading {m_MostRecentTemperatureReading} at {DateTime.Now.ToString()}");
            }
        }


        private double m_MostRecentHumidityReading;
        public double MostRecentHumidityReading
        {
            get
            {
                return m_MostRecentHumidityReading;
            }
            set
            {
                m_MostRecentHumidityReading = value;
                //System.Diagnostics.Debug.WriteLine($"In TCA9548A_I2C_Multiplexer_DiagnosticViewer, Most recent Humidity Reading {m_MostRecentHumidityReading} at {DateTime.Now.ToString()}");
            }
        }

        private void Timer_Tick(object sender, object e)
        {
            //textBlockCurrentTemperatureReading.Text = $"Temperature " + MostRecentTemperatureReading.ToString();
            //textBlockCurrentHumidityReading.Text    = $"   Humidity " + MostRecentHumidityReading.ToString();
        }
        #endregion


        public TCA9548A_I2C_Multiplexer_DiagnosticViewer()
        {
            this.InitializeComponent();
            textPartNumber.Text = htu21d.DevicePartNumberIdentifier;
            textDeviceName.Text = htu21d.DeviceInstanceNickname;

            htu21d.NewSensorReadingEvent += Htu21d_NewSensorReadingEvent;


            // start our time that updates on our UI thread
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Htu21d_NewSensorReadingEvent(object sender, YSS.IoT.Devices.Sensors.Weather.HTU21D.HTU21D.NewSensorReadingEventEventArgs EventArgsParam)
        {
            HTU21D sendingSensor = sender as HTU21D;
            if (sendingSensor != null)
            {
                MostRecentTemperatureReading = EventArgsParam.TemperatureReadingInCelcius;
                MostRecentHumidityReading = EventArgsParam.HumidityReading;
            }
            else
            {
                throw new System.NotImplementedException("Unrecognized sender type cased exception in UltrasonicRangeSensor_NewSensorReadingEvent");
            }

        }

        public string DeviceInstanceNickname
        {
            get { return htu21d.DeviceInstanceNickname; }
            set
            {
                htu21d.DeviceInstanceNickname = value;
                textDeviceName.Text = htu21d.DeviceInstanceNickname;
            }
        }

        private void CheckBoxTriggerWithTimer_Checked(object sender, RoutedEventArgs e)
        {
            if (htu21d != null)
            {
                htu21d.SendSensorDataContinuallyBasedOntimer = checkBoxTriggerWithTimer.IsChecked.GetValueOrDefault(false);
            }
        }

        private void CheckBoxTriggerWithTimer_Unchecked(object sender, RoutedEventArgs e)
        {
            if (htu21d != null)
            {
                htu21d.SendSensorDataContinuallyBasedOntimer = checkBoxTriggerWithTimer.IsChecked.GetValueOrDefault(false);
            }
        }
    }
}
