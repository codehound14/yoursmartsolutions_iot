// Title: Ring Doorbell Pro Wall Mount Customizer.
// Author: https://www.thingiverse.com/pasqo
// License: https://creativecommons.org/licenses/by/3.0/legalcode

// Generate a custom Ring Doorbell Pro wall mount with your
// choice of corner, wedge angles, and other settings.
// The generator can be easily remixed to fit other
// doorbell models.



include <YourSmartSolutions_Constants.scad>
use <YourSmartSolutions_Functions.scad>
use <YourSmartSolutions_Functions_ScrewHoles.scad>
use <threads_v2p1.scad>



/* [Mount Angles] */
// Corner Angle [degree]
corner_deg = 30; // [0:5:100]
// Wedge Angle [degree]
wedge_deg = 5; // [-45:-5:0:5:45]

/* [Strength] */
// Base Thickness [mm]
base_t = 3; // [2:6]
// Wall Thickness [mm]
wall_t = 3; // [2:6]
// Use Brackets
use_brackets = false;

/* [Wall Mounting] */
// Wiring Hole Radius [mm]
base_hole_r = 12;
// Mounting Hole Radius [mm]
base_screw_r = 2; // [0:0.25:4]
// Mounting Hole Distance to Center [mm]
base_screw_y = 27.5;

/* [Details] */
// Has Ridge
has_ridge = true;
// Ridge Thickness [mm]
ridge_t = 1.5;

ShowRainHood=false;

/* [Box Parameters] */
// Box Depth [mm]
BoxDepth = 15; // [0:30]
BoxWidth = 47; // [30:150]
WireChannelRadius = 5; // [2:8]
MetricScrewSize = 3; // [2:5]

// Hide Remaining paramaters.
module mirror_wedge() {
  if(wedge_deg < 0)
    mirror([0,1,0])
    children();
  else
    children();
}

/* [Doorbell Size] */
// Doorbell Height [mm]
base_h = 115; // 
// Doorbell Width [mm]
base_w = 47;

// Rotation Radius [mm]
corner_rotation_r = 10;
wedge_rotation_r = 20;

/* [Doorbell Mounting] */
// Doorbell Mounting Hole Distance to Center [mm]
screw_y = 52.5;
// Doorbell Mounting Hole Radius [mm] 
screw_r = 1;
// Doorbell Mounting Hole Length [mm] 
screw_l = 5;


/* [View Parameters] */
ShowLayingOnBack = true;

/* [Arc Smoothness] */
$fs = 0.8;
$fa = 0.1;

fudge_lin = 1e-03;
fudge_deg = 1e-01;

module profile() {
  difference() {
    square([base_w, base_h], center=true);
    square([base_w-2*wall_t, base_h-2*wall_t], center=true);
  }
  translate([0, +screw_y])
  circle(base_h/2-screw_y);
  translate([0, -screw_y])
  circle(base_h/2-screw_y);
  if (use_brackets) {
    y = base_screw_y+10;
    w = base_w/2-base_hole_r;
    translate([-base_w/2, y-wall_t/2, 0])
    square([base_w, wall_t]);
    translate([-base_w/2, -wall_t/2, 0]) {
      square([w, wall_t]);
      translate([base_w/2+base_hole_r, 0, 0])
      square([w, wall_t]);
    }
    translate([-base_w/2, -y-wall_t/2, 0])
    square([base_w, wall_t]);
  }
}

module plate() {
  rotate([0, -corner_deg, 0])
  translate([+corner_rotation_r+base_w/2, 0, 0])
  rotate([0, 0, 90])
  translate([-wedge_rotation_r-base_h/2, 0, 0])
  rotate([0, -abs(wedge_deg), 0])
  translate([wedge_rotation_r+base_h/2, 0, 0]) {
    translate([0, 0, -screw_l+1]) {
      translate([+screw_y, 0, 0])
      cylinder(h=screw_l+1, r=screw_r);
      translate([-screw_y, 0, 0])
      cylinder(h=screw_l+1, r=screw_r);
    }
    if (has_ridge) {
      difference()
		{
        cube([base_h+4*ridge_t, base_w+4*ridge_t, ridge_t], center=true);
        cube([base_h-2*ridge_t, base_w-2*ridge_t, 2*ridge_t], center=true);
      }
    }
  }
}



module RainHood() {
	SidePanelWidth = 20;
	SidePanelHeight = 40;
	SidePanelThickness = wall_t;
	RoofPitch = 60;
	RoofPanelMoveUp = 60;
	RoofPanelMoveToCenter = -2;
	
  rotate([0, -corner_deg, 0])
  translate([+corner_rotation_r+base_w/2, 0, 0])
  rotate([0, 0, 90])
  translate([-wedge_rotation_r-base_h/2, 0, 0])
  rotate([0, -abs(wedge_deg), 0])
  translate([wedge_rotation_r+base_h/2, 0, 0]) {
    translate([0, 0, -screw_l+1]) {
      translate([+screw_y, 0, 0])
      cylinder(h=screw_l+1, r=screw_r);
      translate([-screw_y, 0, 0])
      cylinder(h=screw_l+1, r=screw_r);
    }
    if (ShowRainHood)
	{
		//if(false)
		{
		// left side of roof
		rotate([0, 0, RoofPitch])
		translate([RoofPanelMoveToCenter, -RoofPanelMoveUp, 0])  
		color("Green", 0.75)
		cube([SidePanelHeight,SidePanelThickness,SidePanelWidth]);
		}
		
		
		//if(false)
		{
		// right side of roof
		rotate([0, 0, -RoofPitch])
		translate([-RoofPanelMoveToCenter, RoofPanelMoveUp, 0])  
		color("Purple", 0.75)
		cube([SidePanelHeight,SidePanelThickness,SidePanelWidth]);
		}
		
		//color("Blue", 0.5)
		//cube([5,30,60]);
		
		
    }
  }
}

module box()
{
	// make sure box width is at least the width of the doorbell
	TotalBoxWidth = max(BoxWidth, base_w);
	
	BoxOverhangWidth = TotalBoxWidth - base_w;
	
	//color("Blue", 0.15)
	
	translate([BoxOverhangWidth/2, 0, -BoxDepth/2])
	
	difference()
	{
		// the main part of the box
		cube([TotalBoxWidth, base_h, BoxDepth], center=true);
		
		
		// Drill Holes to mount box to wall
		ScrewDistanceFromEdgeOfBox = CountersinkScrewHeadOuterDiameter(MetricScrewSize) + MetricScrewSize;
		BoltZOffset = BoxDepth/2 + ExtraHeightToMakeHolesGoThrough;
		BackX = TotalBoxWidth/2 - ScrewDistanceFromEdgeOfBox;
		MidX = -TotalBoxWidth/2 + base_w + ScrewDistanceFromEdgeOfBox;
		LeftY = base_h/2 - ScrewDistanceFromEdgeOfBox;
		RightY = -base_h/2 + ScrewDistanceFromEdgeOfBox;
		color("Red", 0.85)
		{
			// BackLeft Screw Hole
			translate([BackX, LeftY, BoltZOffset])
			{
				rotate([0,180,0])
				{
					BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough);
				}
			}
			
			// BackRight Screw Hole
			translate([BackX, RightY, BoltZOffset])
			{
				rotate([0,180,0])
				{
					BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough);
				}
			}
			
			// MidLeft Screw Hole
			translate([MidX, LeftY, BoltZOffset])
			{
				rotate([0,180,0])
				{
					BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough);
				}
			}
			
			// MidRight Screw Hole
			translate([MidX, RightY, BoltZOffset])
			{
				rotate([0,180,0])
				{
					BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough);
				}
			}
		}		
		
		// cut a channel for wires
		color("Orange", 0.85)
		{
			translate([0,0,-BoxDepth/2])
			{
				cube([TotalBoxWidth - 2*base_t, base_h/2, BoxDepth], center=true);
			}
		}
	//}
		
		
	}
}

module base() {
  translate([corner_rotation_r+base_w/2, 0, -base_t/2])
  difference() {
    union() {
		box();
      cube([base_w, base_h, base_t+fudge_lin], center=true);
      translate([0, +base_screw_y, 0])
      cylinder(h=base_t/2+1, r=base_screw_r+4);
      translate([0, -base_screw_y, 0])
      cylinder(h=base_t/2+1, r=base_screw_r+4);
    }
    union() {
      cylinder(h=2*base_t +BoxDepth*1.75, r=base_hole_r, center=true);
      translate([0, +base_screw_y, 0])
      cylinder(h=2*base_t+1, r=base_screw_r, center=true);
      translate([0, -base_screw_y, 0])
      cylinder(h=2*base_t+1, r=base_screw_r, center=true);
    }
  }
}

module corner() {
  rotate([90, 0, 0])
  rotate_extrude(angle=corner_deg, convexity=10)
  translate([corner_rotation_r+base_w/2, 0, 0])
  profile();
}

module wedge () {
  rotate([0, fudge_deg-corner_deg, 0])
  translate([corner_rotation_r+base_w/2, 0, 0])
  rotate([90, 0, 90])
  translate([-base_h/2-wedge_rotation_r, 0, 0])
  rotate_extrude(angle=abs(wedge_deg), convexity=10)
  translate([wedge_rotation_r+base_h/2, 0, 0])
  rotate([0, 0, 90])
  profile();
}

module mount() {
  mirror_wedge()
  difference() {
    union() {
	  //box();
      base();
      corner();
      wedge();
		RainHood();
    }
    plate();
  }
}

//mount();

module ShowPreview()
{
	
	
	if(ShowLayingOnBack)
	{
		rotate([0, 0, 0])
		translate([0, 0, 0])
		
		mount();
	}
	else
	{
		mount();
	}

}

ShowPreview();

	$vpd = 500;
//$vpd = 500;
//$vpt = [base_w - 40, -30, 20];
//$vpr = [140, 100, -20];


