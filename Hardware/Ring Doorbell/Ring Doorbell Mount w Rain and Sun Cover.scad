// Title: Ring Doorbell Pro Wall Mount Customizer.
// Author: https://www.thingiverse.com/pasqo
// License: https://creativecommons.org/licenses/by/3.0/legalcode

// Generate a custom Ring Doorbell Pro wall mount with your
// choice of corner, wedge angles, and other settings.
// The generator can be easily remixed to fit other
// doorbell models.


//Todo/problems:
//  As Pete approaches a total rewrite, consider if this is still a customization, or maybe derivative, or becomes origional-ish ???
//  Face does not match back of ring doorbell 2 pro
//	make a new face for each type of doorbell and add it on at the end of extrusion of the curved shape
//	refactor so that curve is its own thing, faceplate is own thing, box is own thing, base, etc.
//  Screw holes are too small for doorbell screws
//	switch to pete's metric screw tool
//  Consider adding a house number or welcome message to box/base/face
//  Consider adding hook or hole or something for hanging seasonal decorations
//  Add/improve rain/sun shield / roof
//  Creat debug mode/output that shows each component created along x axis to make them easier to view and debug



include <YourSmartSolutions_Constants.scad>
include <Chamfers-for-OpenSCAD/Chamfer.scad>;
include <YourSmartSolutions_Functions_ThreadedInsertHoles_Include.scad>
use <YourSmartSolutions_Functions.scad>
use <YourSmartSolutions_Functions_ScrewHoles.scad>
use <YourSmartSolutions_Functions_ThreadedInsertHoles_Use.scad>
use <threads_v2p1.scad>


/*
/*
/* Customizer Settings
/*
/*

/* [Viewing and Debugging] */
//The Resolution of the curves. Higher values give smoother curves but may increase the model render time.
Resolution = 64; //[4, 8, 16, 32, 64, 128, 256]
$fn = Resolution;
Show_Combined_Complete_Product = true;
Exclude_Components_For_Combined_Print = true;
Show_OLD_Composite_Parts = false;
Show_Bounding_Box = false;
Show_All_Decomposed_Components = false;
Show_Mounting_Face_For_Ring_Doorbell_Pro_2_On_CoverPlate_Box = false;
Show_Model_Of_Ring_Doorbell_Pro_2 = false;
Show_Mounting_Face_For_Ring_Doorbell_Pro_2 = false;
Show_Template_For_WallMounting_Screw_Placement = false;
Show_WallMounting_Base = false;
Show_WallMounting_CoverPlate_For_Covering_Base = false;
Show_WallMounting_CoverPlate_Box = false;
RemoveChunksForFasterPrototypePrinting = false;
SkipAllChamfers = true;

/* [Ring Doorbell Pro 2 Parameters] */
Ring_Doorbell_Pro_2_Metric_Screw_Size = 3; // [3,3]
Ring_Doorbell_Pro_2_Metric_Screw_Length = 28; // [28,28]
Ring_Doorbell_Pro_2_Metric_Screw_Hole_Edge_To_Box_Edge = 1.15; 
Ring_Doorbell_Pro_2_Core_Height = 111;
Ring_Doorbell_Pro_2_Core_Width = 46.5;
Ring_Doorbell_Pro_2_Outer_Plate_Width = 49.5;
Ring_Doorbell_Pro_2_Outer_Plate_Height = 114;
Ring_Doorbell_Pro_2_Outer_Plate_Depth = 18;
Ring_Doorbell_Pro_2_Outer_Plate_Wall_Thickness = 1.8;
Ring_Doorbell_Pro_2_Depth_Inner_Protrusion = 5.75;


Ring_Doorbell_Pro_2_Mounting_Face_Thickness = 9; // [1:0.25:9]
Ring_Doorbell_Pro_2_Mounting_Face_Border = 7; // [0:1:15]
Ring_Doorbell_Pro_2_Mounting_Face_Wiggle_Room_To_Allow_Doorbell_To_Slip_In = 0.5; // [0:0.1:1]
Ring_Doorbell_Pro_2_Mounting_Face_Height = Ring_Doorbell_Pro_2_Core_Height + Ring_Doorbell_Pro_2_Mounting_Face_Border *2;
Ring_Doorbell_Pro_2_Mounting_Face_Width = Ring_Doorbell_Pro_2_Core_Width + Ring_Doorbell_Pro_2_Mounting_Face_Border*2;


/* [Mount Angles] */
// Corner Angle [degree]
corner_deg = 80; // [0:5:100]
// Wedge Angle [degree]
wedge_deg = -3; // [-45:-5:0:5:45]

/* [Strength] */
// Base Thickness [mm]
base_t = 3; // [2:6]
// Wall Thickness [mm]
wall_t = 3; // [2:6]
// Use Brackets
use_brackets = false;

/* [Wall Mounting] */
// Wiring Hole Radius [mm]
base_hole_r = 12;
// Mounting Hole Radius [mm]
base_screw_r = 2; // [0:0.25:4]
// Mounting Hole Distance to Center [mm]
base_screw_y = 27.5;

ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls=0.75;

WallMounting_Base_Height=115;
WallMounting_Base_Width=100;
WallMounting_Base_Thickness=10;

WallMounting_Base_Channel_Height=70;
WallMounting_Base_Channel_Width=WallMounting_Base_Width;
WallMounting_Base_Channel_Thickness=5;
WallMounting_Base_Channel_WidthOfWallToLeave = 5;

// Enough space to allow plate to CoverPlate to slide over base
WallMounting_GapBetweenBaseAndPlate = 0.2; 

WallMounting_CoverPlate_BorderAroundBase = 5;
WallMounting_CoverPlate_Height = WallMounting_Base_Height + 2*WallMounting_CoverPlate_BorderAroundBase;
WallMounting_CoverPlate_Width = 140;
WallMounting_CoverPlate_Thickness = WallMounting_Base_Thickness + WallMounting_CoverPlate_BorderAroundBase;
WallMounting_CoverPlate_CornerHook_AddToModel = true;
WallMounting_CoverPlate_CornerHook_Width = 10;
WallMounting_CoverPlate_CornerHook_Thickness = 4;


WallMounting_CoverPlate_Box_Height=Ring_Doorbell_Pro_2_Mounting_Face_Height;
WallMounting_CoverPlate_Box_Width=Ring_Doorbell_Pro_2_Mounting_Face_Width;
WallMounting_CoverPlate_Box_Thickness=25;

WallMounting_ScrewInsertsPattern_WidthOfScrewCenters = 40;
WallMounting_ScrewInsertsPattern_HeightOfScrewCenters = 60;
WallMounting_ScrewInsertsPattern_OffsetFromLeft = 32;

WallMounting_ScrewPattern_WidthOfScrewCenters = 40;
WallMounting_ScrewPattern_HeightOfScrewCenters = 103.5;
WallMounting_ScrewPattern_OffsetFromLeft = 32;

WallMounting_CoverPlate_Box_CutOutChannelForWires = true;
WallMounting_CoverPlate_Box_CutOutChannelForWires_ThroughRight = true;
WallMounting_CoverPlate_Box_CutOutChannelForWires_ThroughBack = true;

/* [Details] */
// Has Ridge
has_ridge = false;
// Ridge Thickness [mm]
ridge_t = 1.5;

ShowRainHood=false;

/* [Box Parameters] */
// Box Depth [mm]
BoxDepth = 15; // [0:30]
BoxWidth = 114; // [30:150]
MetricScrewSize = 3; // [2:5]
MetricScrewSize_ForTemplateWoodScrewHoles = 2; //[1.5:4.5]
BoxScrewDistanceFromBack = Ring_Doorbell_Pro_2_Outer_Plate_Width/2;

// Hide Remaining paramaters.
module mirror_wedge() {
  if(wedge_deg < 0)
    mirror([0,1,0])
    children();
  else
    children();
}

/* [Doorbell Size] */
// Doorbell Height [mm]
base_h = 115; // 
// Doorbell Width [mm]
base_w = 47;

// Rotation Radius [mm]
corner_rotation_r = 10;
wedge_rotation_r = 20;

/* [Doorbell Mounting] */
// Doorbell Mounting Hole Distance to Center [mm]
screw_y = 52.5;
// Doorbell Mounting Hole Radius [mm] 
screw_r = 1;
// Doorbell Mounting Hole Length [mm] 
screw_l = 5;


/* [View Parameters] */
ShowLayingOnBack = false;

/* [Arc Smoothness] */
$fs = 0.8;
$fa = 0.1;

fudge_lin = 1e-03;
fudge_deg = 1e-01;

module profile() {
  difference() {
    square([base_w, base_h], center=true);
    square([base_w-2*wall_t, base_h-2*wall_t], center=true);
  }
  translate([0, +screw_y])
  circle(base_h/2-screw_y);
  translate([0, -screw_y])
  circle(base_h/2-screw_y);
  if (use_brackets) {
    y = base_screw_y+10;
    w = base_w/2-base_hole_r;
    translate([-base_w/2, y-wall_t/2, 0])
    square([base_w, wall_t]);
    translate([-base_w/2, -wall_t/2, 0]) {
      square([w, wall_t]);
      translate([base_w/2+base_hole_r, 0, 0])
      square([w, wall_t]);
    }
    translate([-base_w/2, -y-wall_t/2, 0])
    square([base_w, wall_t]);
  }
}

module plate()
{
	rotate([0, -corner_deg, 0])
	translate([+corner_rotation_r+base_w/2, 0, 0])
	rotate([0, 0, 90])
	translate([-wedge_rotation_r-base_h/2, 0, 0])
	rotate([0, -abs(wedge_deg), 0])
	translate([wedge_rotation_r+base_h/2, 0, 0])
	{
		translate([0, 0, -screw_l+1])
		{
			translate([+screw_y, 0, 0])
				cylinder(h=screw_l+1, r=screw_r);
			translate([-screw_y, 0, 0])
				cylinder(h=screw_l+1, r=screw_r);
		}
		if (has_ridge)
		{
			difference()
			{
				cube([base_h+4*ridge_t, base_w+4*ridge_t, ridge_t], center=true);
				cube([base_h-2*ridge_t, base_w-2*ridge_t, 2*ridge_t], center=true);
			}
		}
	}
}



module RainHood() {
	SidePanelWidth = 20;
	SidePanelHeight = 40;
	SidePanelThickness = wall_t;
	RoofPitch = 60;
	RoofPanelMoveUp = 60;
	RoofPanelMoveToCenter = -2;
	
  rotate([0, -corner_deg, 0])
  translate([+corner_rotation_r+base_w/2, 0, 0])
  rotate([0, 0, 90])
  translate([-wedge_rotation_r-base_h/2, 0, 0])
  rotate([0, -abs(wedge_deg), 0])
  translate([wedge_rotation_r+base_h/2, 0, 0]) {
    translate([0, 0, -screw_l+1]) {
      translate([+screw_y, 0, 0])
      cylinder(h=screw_l+1, r=screw_r);
      translate([-screw_y, 0, 0])
      cylinder(h=screw_l+1, r=screw_r);
    }
    if (ShowRainHood)
	{
		//if(false)
		{
		// left side of roof
		rotate([0, 0, RoofPitch])
		translate([RoofPanelMoveToCenter, -RoofPanelMoveUp, 0])  
		color("Green", 0.75)
		cube([SidePanelHeight,SidePanelThickness,SidePanelWidth]);
		}
		
		
		//if(false)
		{
		// right side of roof
		rotate([0, 0, -RoofPitch])
		translate([-RoofPanelMoveToCenter, RoofPanelMoveUp, 0])  
		color("Purple", 0.75)
		cube([SidePanelHeight,SidePanelThickness,SidePanelWidth]);
		}
		
		//color("Blue", 0.5)
		//cube([5,30,60]);
		
		
    }
  }
}

module box()
{
	// make sure box width is at least the width of the doorbell
	TotalBoxWidth = max(BoxWidth, base_w);
	
	BoxOverhangWidth = TotalBoxWidth - base_w;
	
	//color("Blue", 0.15)
	
	translate([BoxOverhangWidth/2, 0, -BoxDepth/2])
	
	difference()
	{
		// the main part of the box
		cube([TotalBoxWidth, base_h, BoxDepth], center=true);
		
		
		// Drill Holes to mount box to wall
		ScrewDistanceFromEdgeOfBox = CountersinkScrewHeadOuterDiameter(MetricScrewSize) + MetricScrewSize;
		BoltZOffset = BoxDepth/2 + ExtraHeightToMakeHolesGoThrough;
		BackX = TotalBoxWidth/2 - max(ScrewDistanceFromEdgeOfBox, BoxScrewDistanceFromBack);
		MidX = -TotalBoxWidth/2 + base_w + ScrewDistanceFromEdgeOfBox;
		LeftY = base_h/2 - ScrewDistanceFromEdgeOfBox;
		RightY = -base_h/2 + ScrewDistanceFromEdgeOfBox;
		ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls=0.75;
		color("Red", 0.85)
		{
			// BackLeft Screw Hole
			translate([BackX, LeftY, BoltZOffset])
			{
				rotate([0,180,0])
				{
					#BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
					ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
						 NumberOfSides = Resolution);
				}
			}
			
			// BackRight Screw Hole
			translate([BackX, RightY, BoltZOffset])
			{
				rotate([0,180,0])
				{
					BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
					ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
						 NumberOfSides = Resolution);
				}
			}
			
			// MidLeft Screw Hole
			translate([MidX, LeftY, BoltZOffset])
			{
				rotate([0,180,0])
				{
					BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
					ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
						 NumberOfSides = Resolution);
				}
			}
			
			// MidRight Screw Hole
			translate([MidX, RightY, BoltZOffset])
			{
				rotate([0,180,0])
				{
					BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
					ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
						 NumberOfSides = Resolution);
				}
			}
		}		
		
		// cut a channel for wires
		color("Orange", 0.85)
		{
			translate([0,0,-BoxDepth/2])
			{
				cube([TotalBoxWidth - 2*base_t, base_h/2, BoxDepth], center=true);
			}
		}
	//}
		
		
	}
}

module base() {
  translate([corner_rotation_r+base_w/2, 0, -base_t/2])
  difference() {
    union() {
		box();
      cube([base_w, base_h, base_t+fudge_lin], center=true);
      translate([0, +base_screw_y, 0])
      cylinder(h=base_t/2+1, r=base_screw_r+4);
      translate([0, -base_screw_y, 0])
      cylinder(h=base_t/2+1, r=base_screw_r+4);
    }
    #union() {
      cylinder(h=2*base_t +BoxDepth*1.75, r=base_hole_r, center=true);
      translate([0, +base_screw_y, 0])
      cylinder(h=2*base_t+1, r=base_screw_r, center=true);
      translate([0, -base_screw_y, 0])
      cylinder(h=2*base_t+1, r=base_screw_r, center=true);
    }
  }
}

module corner() {
  rotate([90, 0, 0])
  rotate_extrude(angle=corner_deg, convexity=10)
  translate([corner_rotation_r+base_w/2, 0, 0])
  profile();
}

module wedge () {
  rotate([0, fudge_deg-corner_deg, 0])
  translate([corner_rotation_r+base_w/2, 0, 0])
  rotate([90, 0, 90])
  translate([-base_h/2-wedge_rotation_r, 0, 0])
  rotate_extrude(angle=abs(wedge_deg), convexity=10)
  translate([wedge_rotation_r+base_h/2, 0, 0])
  rotate([0, 0, 90])
  profile();
}


/// Starting out pretty blocky.  Can refine later if desired
module Model_Of_Ring_Doorbell_Pro_2()
{
	// Silver Front Cover Plate 
	color("Silver", 0.95)
	{
		cube([Ring_Doorbell_Pro_2_Outer_Plate_Width,
			Ring_Doorbell_Pro_2_Outer_Plate_Depth,
			Ring_Doorbell_Pro_2_Outer_Plate_Height]);
	}

	// Black main electrical component
	//color("Black", 0.75)
	{
		translate([
			Ring_Doorbell_Pro_2_Outer_Plate_Wall_Thickness -Ring_Doorbell_Pro_2_Mounting_Face_Wiggle_Room_To_Allow_Doorbell_To_Slip_In,
			Ring_Doorbell_Pro_2_Outer_Plate_Depth,
			Ring_Doorbell_Pro_2_Outer_Plate_Wall_Thickness -Ring_Doorbell_Pro_2_Mounting_Face_Wiggle_Room_To_Allow_Doorbell_To_Slip_In])
		{
			cube([Ring_Doorbell_Pro_2_Core_Width + 2*Ring_Doorbell_Pro_2_Mounting_Face_Wiggle_Room_To_Allow_Doorbell_To_Slip_In,
				Ring_Doorbell_Pro_2_Depth_Inner_Protrusion,
				Ring_Doorbell_Pro_2_Core_Height +2*Ring_Doorbell_Pro_2_Mounting_Face_Wiggle_Room_To_Allow_Doorbell_To_Slip_In]);

			
			// Add Screws
			{
				ScrewDistanceFromEdgeOfBox = Ring_Doorbell_Pro_2_Metric_Screw_Size/2 + Ring_Doorbell_Pro_2_Metric_Screw_Hole_Edge_To_Box_Edge;
				
				BoltZOffset = 0; //BoxDepth/2 + ExtraHeightToMakeHolesGoThrough;
				ScrewX = Ring_Doorbell_Pro_2_Core_Width/2;
				TopScrewY = 0; // Ring_Doorbell_Pro_2_Depth_Inner_Protrusion;
				LeftY = base_h/2 - ScrewDistanceFromEdgeOfBox;
				RightY = -base_h/2 + ScrewDistanceFromEdgeOfBox;
				ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls=0.75;
				
				// Top Screw
				translate([ScrewX, TopScrewY, Ring_Doorbell_Pro_2_Core_Height - ScrewDistanceFromEdgeOfBox])
				{
					rotate([-90,0,0])
					{
						BoltHole_Metric(
						DrillTheCountersink = true,
						MetricScrewSize = MetricScrewSize,
						TotalBoltLength=Ring_Doorbell_Pro_2_Metric_Screw_Length + 2*ExtraHeightToMakeHolesGoThrough,
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
							ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
						 NumberOfSides = Resolution);
					}
				}

				// Bottom Screw
				translate([ScrewX, TopScrewY, ScrewDistanceFromEdgeOfBox])
				{
					rotate([-90,0,0])
					{
						BoltHole_Metric(
						DrillTheCountersink = true,
						MetricScrewSize = MetricScrewSize,
						TotalBoltLength=Ring_Doorbell_Pro_2_Metric_Screw_Length + 2*ExtraHeightToMakeHolesGoThrough,
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
							ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
						 NumberOfSides = Resolution);
					}
				}
			}
		}
	}
}



module Mounting_Face_For_Ring_Doorbell_Pro_2()
{
	difference()
	{
		cube([Ring_Doorbell_Pro_2_Mounting_Face_Width,
			Ring_Doorbell_Pro_2_Mounting_Face_Thickness,
			Ring_Doorbell_Pro_2_Mounting_Face_Height]);
		
		// remove impression of actual doorbell model
		translate([Ring_Doorbell_Pro_2_Mounting_Face_Width/2 - Ring_Doorbell_Pro_2_Outer_Plate_Width/2,
			-Ring_Doorbell_Pro_2_Outer_Plate_Depth - ExtraHeightToMakeHolesGoThrough*2,
			Ring_Doorbell_Pro_2_Mounting_Face_Height/2 - Ring_Doorbell_Pro_2_Outer_Plate_Height/2])
		{
			Model_Of_Ring_Doorbell_Pro_2();
		}
		
		// remove cube shaped hole for wires to go through		
		WidthOfWireHole = 30;
		HeightOfWireHole = 75;
		//ThicknessOfWireHoleCube = Ring_Doorbell_Pro_2_Mounting_Face_Thickness*2 + Ring_Doorbell_Pro_2_Depth_Inner_Protrusion*2 ;
		ThicknessOfWireHoleCube = Ring_Doorbell_Pro_2_Outer_Plate_Depth * 1.25;
		translate([Ring_Doorbell_Pro_2_Mounting_Face_Width/2 - WidthOfWireHole/2,
			-Ring_Doorbell_Pro_2_Outer_Plate_Depth/2 - ExtraHeightToMakeHolesGoThrough*2,
			Ring_Doorbell_Pro_2_Mounting_Face_Height/2 - HeightOfWireHole/2])
		{
			cube([WidthOfWireHole,
				ThicknessOfWireHoleCube,
				HeightOfWireHole]);
		}
		
	}
}




module mount()
{
	mirror_wedge()
	{
		difference()
		{
			union()
			{
				// we no longer use the box //box();
				base();
				corner();
				wedge();
				RainHood();
			}

			plate();

			

			// Subtract Model to cut holes for screws 
			{
				rotate([0, -corner_deg, 0])
				translate([+corner_rotation_r+base_w/2, 0, 0])
				rotate([0, 0, 90])
				translate([-wedge_rotation_r-base_h/2, 0, 0])
				rotate([0, -abs(wedge_deg), 0])
				translate([wedge_rotation_r+base_h/2, 0, 0])
				{
					rotate([90, 180, 90])
					{
						translate([-Ring_Doorbell_Pro_2_Outer_Plate_Width/2 , 
							-Ring_Doorbell_Pro_2_Outer_Plate_Depth - Ring_Doorbell_Pro_2_Mounting_Face_Thickness,
							-Ring_Doorbell_Pro_2_Outer_Plate_Height/2])
						{
							//Model_Of_Ring_Doorbell_Pro_2();
						}
					}
				}
			}





		}

		// Add mounting plate
		{
			rotate([0, -corner_deg, 0])
			translate([+corner_rotation_r+base_w/2, 0, 0])
			rotate([0, 0, 90])
			translate([-wedge_rotation_r-base_h/2, 0, 0])
			rotate([0, -abs(wedge_deg), 0])
			translate([wedge_rotation_r+base_h/2, 0, Ring_Doorbell_Pro_2_Mounting_Face_Thickness])
			{
				rotate([90, 180, 90])
				{
					translate([-Ring_Doorbell_Pro_2_Core_Width/2 - Ring_Doorbell_Pro_2_Mounting_Face_Border, 
						0,
						-Ring_Doorbell_Pro_2_Core_Height/2 - Ring_Doorbell_Pro_2_Mounting_Face_Border])
					{
						Mounting_Face_For_Ring_Doorbell_Pro_2();
					}
				}
			}
		}
	}
	
}

//mount();
module bbox() { 

    // a 3D approx. of the children projection on X axis 
    module xProjection() 
        translate([0,1/2,-1/2]) 
            linear_extrude(1) 
                hull() 
                    projection() 
                        rotate([90,0,0]) 
                            linear_extrude(1) 
                                projection() children(); 
  
    // a bounding box with an offset of 1 in all axis
    module bbx()  
        minkowski() { 
            xProjection() children(); // x axis
            rotate(-90)               // y axis
                xProjection() rotate(90) children(); 
            rotate([0,-90,0])         // z axis
                xProjection() rotate([0,90,0]) children(); 
        } 
    
    // offset children() (a cube) by -1 in all axis
    module shrink()
      intersection() {
        translate([ 1, 1, 1]) children();
        translate([-1,-1,-1]) children();
      }

   shrink() bbx() children(); 
}

module ShowPreview()
{
	GapBetweenPreviewModels = 15;
	Show_Model_Of_Ring_Doorbell_Pro_2__Preview_X = -75;
	Show_Mounting_Face_For_Ring_Doorbell_Pro_2__Preview_X = Show_Model_Of_Ring_Doorbell_Pro_2__Preview_X - Ring_Doorbell_Pro_2_Mounting_Face_Width - GapBetweenPreviewModels;
	Show_WallMounting_CoverPlate_Box__Preview_X = Show_Mounting_Face_For_Ring_Doorbell_Pro_2__Preview_X - WallMounting_CoverPlate_Box_Width - GapBetweenPreviewModels;
	Show_WallMounting_Base__Preview_X  = Show_WallMounting_CoverPlate_Box__Preview_X - WallMounting_Base_Width - GapBetweenPreviewModels;
	Show_WallMounting_CoverPlate_For_Covering_Base__Preview_X = Show_WallMounting_Base__Preview_X - WallMounting_CoverPlate_Width - GapBetweenPreviewModels;
	Show_Template_For_WallMounting_Screw_Placement__Preview_X = Show_WallMounting_CoverPlate_For_Covering_Base__Preview_X - WallMounting_CoverPlate_Width - GapBetweenPreviewModels;
	
	assert(!ShowRainHood, "Rain Hood is not implemented yet");
	
	
	if(Show_OLD_Composite_Parts)
	{
		if(ShowLayingOnBack)
		{
			rotate([0, 0, 0])
			translate([0, 0, 0])
			
			mount();
		}
		else
		{
			mount();
		}
		if(Show_Bounding_Box)
		{
			%bbox() mount();
		}
	}
	
	if(Show_Combined_Complete_Product)
	{		
		//translate([-150, -150, 0])
		{
			if(ShowLayingOnBack)
			{
				rotate([45, 0, 0])
				translate([40, 0, 50])
				
				CompleteCompositeDoorbellModel();
			}
			else
			{
				CompleteCompositeDoorbellModel();
			}
			if(Show_Bounding_Box)
			{
				%bbox() CompleteCompositeDoorbellModel();
			}
		}
			
	}
	
	if(Show_Model_Of_Ring_Doorbell_Pro_2
		|| Show_All_Decomposed_Components)
	{
		translate([Show_Model_Of_Ring_Doorbell_Pro_2__Preview_X, 0, 0])
		{
			Model_Of_Ring_Doorbell_Pro_2();
		}
	}
	
	if(Show_Mounting_Face_For_Ring_Doorbell_Pro_2
		|| Show_All_Decomposed_Components)
	{
		translate([Show_Mounting_Face_For_Ring_Doorbell_Pro_2__Preview_X, 0, 0])
		{
			rotate([-90, 0, 0]) // lay flat on back
			{
				Mounting_Face_For_Ring_Doorbell_Pro_2();
			}
		}
	}

	
	if(Show_WallMounting_CoverPlate_Box
		|| Show_All_Decomposed_Components)
	{
		translate([Show_WallMounting_CoverPlate_Box__Preview_X, 0, 0])
		{
			//rotate([-90, 0, 0]) // lay flat on back
			{
				WallMounting_CoverPlate_Box(CutOutChannelForWires = WallMounting_CoverPlate_Box_CutOutChannelForWires,  RemoveChunksForFasterPrototypePrinting = RemoveChunksForFasterPrototypePrinting);
			}
		}
	}



	
	if(Show_Mounting_Face_For_Ring_Doorbell_Pro_2_On_CoverPlate_Box
		|| Show_All_Decomposed_Components)
	{
		translate([Show_WallMounting_CoverPlate_Box__Preview_X, -150, WallMounting_CoverPlate_Box_Thickness + Ring_Doorbell_Pro_2_Mounting_Face_Thickness +0])
		{
			rotate([-90, 0, 0]) // lay flat on back
			{
				// Mounting_Face_For_Ring_Doorbell_Pro_2();
			}
		}
		
		translate([Show_WallMounting_CoverPlate_Box__Preview_X, -WallMounting_CoverPlate_Box_Height, 0])
		{
			Mounting_Face_For_Ring_Doorbell_Pro_2_On_CoverPlate_Box();
			// //rotate([-90, 0, 0]) // lay flat on back
			// {
			// 	WallMounting_CoverPlate_Box(CutOutChannelForWires = WallMounting_CoverPlate_Box_CutOutChannelForWires,  RemoveChunksForFasterPrototypePrinting = RemoveChunksForFasterPrototypePrinting);
			// }
		}
	}




	
	if(Show_Template_For_WallMounting_Screw_Placement
		|| Show_All_Decomposed_Components)
	{
		translate([Show_Template_For_WallMounting_Screw_Placement__Preview_X, 0, 0])
		{
			//rotate([-90, 0, 0]) // lay flat on back
			{
				TemplateForMarkingWallMountingSrewLocations();
			}
		}
	}

	
	if(Show_WallMounting_Base
		|| Show_All_Decomposed_Components)
	{
		translate([Show_WallMounting_Base__Preview_X, 0, 0])
		{
			//rotate([-90, 0, 0]) // lay flat on back
			{
				WallMounting_Base(RemoveChunksForFasterPrototypePrinting = RemoveChunksForFasterPrototypePrinting);
			}
		}
	}

	
	if(Show_WallMounting_CoverPlate_For_Covering_Base
		|| Show_All_Decomposed_Components)
	{
		translate([Show_WallMounting_CoverPlate_For_Covering_Base__Preview_X, 0, 0])
		{
			//rotate([-90, 0, 0]) // lay flat on back
			{
				WallMounting_CoverPlate_For_Covering_Base(RemoveChunksForFasterPrototypePrinting = RemoveChunksForFasterPrototypePrinting);
			}
		}
	}

}

module CompleteCompositeDoorbellModel()
{
	// Add base
	if(!Exclude_Components_For_Combined_Print)
	{
		translate([WallMounting_CoverPlate_BorderAroundBase + WallMounting_GapBetweenBaseAndPlate, 0 , 0])
		{
			WallMounting_Base(RemoveChunksForFasterPrototypePrinting = RemoveChunksForFasterPrototypePrinting);
		}
	}
	

	// Add plate
	translate([0, - WallMounting_CoverPlate_BorderAroundBase , 0])
	{
		WallMounting_CoverPlate_For_Covering_Base(RemoveChunksForFasterPrototypePrinting = RemoveChunksForFasterPrototypePrinting);
	}

	// Move the mounting face to match width of cover plate extention from the hook inclusion
	HookWidthToAdd = WallMounting_CoverPlate_CornerHook_AddToModel
		? 
			WallMounting_CoverPlate_CornerHook_Width
		:
			0
		;

	
	// add box with mounting plate		
	translate([
		WallMounting_CoverPlate_Width + HookWidthToAdd - Ring_Doorbell_Pro_2_Mounting_Face_Thickness - WallMounting_CoverPlate_Box_Thickness,
		WallMounting_CoverPlate_Height/2 - WallMounting_CoverPlate_Box_Height/2 - WallMounting_CoverPlate_BorderAroundBase,
		WallMounting_CoverPlate_Box_Width + WallMounting_CoverPlate_Thickness])
	{
		rotate([0, 90, 0])
		{
			Mounting_Face_For_Ring_Doorbell_Pro_2_On_CoverPlate_Box();
		}
	}
	
	// Add Doorbell model
	if(!Exclude_Components_For_Combined_Print)
	{
		translate([
			WallMounting_CoverPlate_Width + HookWidthToAdd + Ring_Doorbell_Pro_2_Outer_Plate_Depth,
			0,
			WallMounting_CoverPlate_Box_Width/2 + Ring_Doorbell_Pro_2_Outer_Plate_Width/2 + WallMounting_CoverPlate_Thickness])
		{
			rotate([0, 90, 90])
			{
				Model_Of_Ring_Doorbell_Pro_2();
			}
		}
	}

}


module Mounting_Face_For_Ring_Doorbell_Pro_2_On_CoverPlate_Box()
{	
	translate([0, 0, WallMounting_CoverPlate_Box_Thickness + Ring_Doorbell_Pro_2_Mounting_Face_Thickness])
	{
		rotate([-90, 0, 0]) // lay flat on back
		{
			Mounting_Face_For_Ring_Doorbell_Pro_2();
		}
	}	
	WallMounting_CoverPlate_Box(CutOutChannelForWires = WallMounting_CoverPlate_Box_CutOutChannelForWires,  RemoveChunksForFasterPrototypePrinting = RemoveChunksForFasterPrototypePrinting);
}


module WallMounting_CoverPlate_Box(
	Drill_Screws_Holes = true, // drill screw holes to force stronger print due to walls vs. infil
	Drill_BrassScrewInserts_Holes = true, // don't wan't insert holes removed when plate is removing base or it will leave material behind where the hole was
	RemoveChunksForFasterPrototypePrinting = true,
	CutOutChannelForWires = false,
	SkipAllChamfers = SkipAllChamfers)
{
	// Either skip all Chamfers or use custom chamfer settings
	ChamferSelection = SkipAllChamfers
		? 
			[[0, 0, 0, 0],
			[0, 0, 0, 0],
			[0, 0, 0, 0]]
		:
			[[1, 1, 1, 1],
			[1, 1, 1, 1],
			[1, 1, 1, 1]]
		;


	difference()
	{
		union()
		{
			// add main cube
			//#cube([WallMounting_CoverPlate_Box_Width, WallMounting_CoverPlate_Box_Height, WallMounting_CoverPlate_Box_Thickness]);
			
			//color("blue", 0.1)
			chamferCube(
				[WallMounting_CoverPlate_Box_Width, WallMounting_CoverPlate_Box_Height, WallMounting_CoverPlate_Box_Thickness],
				ch = 0.5,
				// chamfer top and sides to reduce sharp feal
				chamfers = ChamferSelection);

			// add some shape to ease angles
			// maybe just a triangle or something
		}

		// cut out for wires
		if(CutOutChannelForWires)
		{
			WallMounting_CoverPlate_Box_Channel_WidthOfWallToLeave = 8;
			WallMounting_CoverPlate_Box_Channel_Height=WallMounting_CoverPlate_Box_Height - WallMounting_CoverPlate_Box_Channel_WidthOfWallToLeave*4;
			WallMounting_CoverPlate_Box_Channel_Width=WallMounting_CoverPlate_Box_Width - WallMounting_CoverPlate_Box_Channel_WidthOfWallToLeave*2;
			WallMounting_CoverPlate_Box_Channel_Thickness=WallMounting_CoverPlate_Box_Thickness - WallMounting_CoverPlate_Box_Channel_WidthOfWallToLeave*2;

			// start with hollowing out space behind the cover plate
			translate([
				WallMounting_CoverPlate_Box_Channel_WidthOfWallToLeave,
				WallMounting_CoverPlate_Box_Channel_WidthOfWallToLeave*2 -ExtraHeightToMakeHolesGoThrough,
				WallMounting_CoverPlate_Box_Channel_WidthOfWallToLeave -ExtraHeightToMakeHolesGoThrough])
			{
				cube([
					WallMounting_CoverPlate_Box_Channel_Width + ExtraHeightToMakeHolesGoThrough,
					WallMounting_CoverPlate_Box_Channel_Height + ExtraHeightToMakeHolesGoThrough,
					WallMounting_CoverPlate_Box_Channel_Thickness + ExtraHeightToMakeHolesGoThrough ]);
			}

			// now remove top
			translate([
				WallMounting_CoverPlate_Box_Channel_WidthOfWallToLeave,
				WallMounting_CoverPlate_Box_Channel_WidthOfWallToLeave*2 -ExtraHeightToMakeHolesGoThrough,
				WallMounting_CoverPlate_Box_Thickness - WallMounting_CoverPlate_Box_Channel_Thickness + ExtraHeightToMakeHolesGoThrough])
			{
				cube([
					WallMounting_CoverPlate_Box_Channel_Width + ExtraHeightToMakeHolesGoThrough,
					WallMounting_CoverPlate_Box_Channel_Height + ExtraHeightToMakeHolesGoThrough,
					WallMounting_CoverPlate_Box_Channel_Thickness + ExtraHeightToMakeHolesGoThrough ]);
			}

			// now remove right Side
			translate([
				WallMounting_CoverPlate_Box_Width - WallMounting_CoverPlate_Box_Channel_Width + ExtraHeightToMakeHolesGoThrough,
				WallMounting_CoverPlate_Box_Channel_WidthOfWallToLeave -ExtraHeightToMakeHolesGoThrough,
				WallMounting_CoverPlate_Box_Channel_WidthOfWallToLeave -ExtraHeightToMakeHolesGoThrough])
			{
				cube([
					WallMounting_CoverPlate_Box_Channel_Width + ExtraHeightToMakeHolesGoThrough,
					WallMounting_CoverPlate_Box_Channel_Height + ExtraHeightToMakeHolesGoThrough,
					WallMounting_CoverPlate_Box_Channel_Thickness + ExtraHeightToMakeHolesGoThrough ]);
			}
		}

		// cut out for speed
		if(RemoveChunksForFasterPrototypePrinting)
		{
			
			WallMounting_CoverPlate_Box_ChunkRemoval_HeightOfWallToLeave = 5;
			WallMounting_CoverPlate_Box_ChunkRemoval_WidthOfWallToLeave = 7;
			WallMounting_CoverPlate_Box_ChunkRemoval_ThicknessOfWallToLeave = 3;

			// remove core leaving penetration for width
			translate([
				- ExtraHeightToMakeHolesGoThrough,
				WallMounting_CoverPlate_Box_ChunkRemoval_HeightOfWallToLeave - ExtraHeightToMakeHolesGoThrough,
				WallMounting_CoverPlate_Box_ChunkRemoval_ThicknessOfWallToLeave - ExtraHeightToMakeHolesGoThrough])
			{
				cube([
					WallMounting_CoverPlate_Box_Width + 2*ExtraHeightToMakeHolesGoThrough,
					WallMounting_CoverPlate_Box_Height + ExtraHeightToMakeHolesGoThrough - 2*WallMounting_CoverPlate_Box_ChunkRemoval_HeightOfWallToLeave,
					WallMounting_CoverPlate_Box_Thickness + ExtraHeightToMakeHolesGoThrough - 2*WallMounting_CoverPlate_Box_ChunkRemoval_ThicknessOfWallToLeave]);
			}

			// remove core leaving penetration for height
			translate([
				WallMounting_CoverPlate_Box_ChunkRemoval_WidthOfWallToLeave - ExtraHeightToMakeHolesGoThrough,
				- ExtraHeightToMakeHolesGoThrough,
				WallMounting_CoverPlate_Box_ChunkRemoval_ThicknessOfWallToLeave -ExtraHeightToMakeHolesGoThrough])
			{
				cube([
					WallMounting_CoverPlate_Box_Width + 2*ExtraHeightToMakeHolesGoThrough - 2*WallMounting_CoverPlate_Box_ChunkRemoval_WidthOfWallToLeave,
					WallMounting_CoverPlate_Box_Height + 2*ExtraHeightToMakeHolesGoThrough,
					WallMounting_CoverPlate_Box_Thickness + ExtraHeightToMakeHolesGoThrough - 2*WallMounting_CoverPlate_Box_ChunkRemoval_ThicknessOfWallToLeave]);
			}

			// remove core leaving penetration for thickness
			translate([
				WallMounting_CoverPlate_Box_ChunkRemoval_WidthOfWallToLeave - ExtraHeightToMakeHolesGoThrough,
				WallMounting_CoverPlate_Box_ChunkRemoval_HeightOfWallToLeave - ExtraHeightToMakeHolesGoThrough,
				-ExtraHeightToMakeHolesGoThrough])
			{
				cube([
					WallMounting_CoverPlate_Box_Width + 2*ExtraHeightToMakeHolesGoThrough - 2*WallMounting_CoverPlate_Box_ChunkRemoval_WidthOfWallToLeave,
					WallMounting_CoverPlate_Box_Height + ExtraHeightToMakeHolesGoThrough - 2*WallMounting_CoverPlate_Box_ChunkRemoval_HeightOfWallToLeave,
					WallMounting_CoverPlate_Box_Thickness + 2*ExtraHeightToMakeHolesGoThrough]);
			}
		}
	}
}

module TempNotes()
{
	// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Modifier_Characters
	// % - Ingore in actual subtree but preview as grey
	// # - Render in pink even if removed as part of diff
	// ! - Make this SubTree the new root (ignore everthing above/before
	// * - Disable SubTree

	//
	//
	//// Set Viewport to show item we are currently debugging
	//$vpt = [ -160.57, -63.78, 21.19 ];
	//$vpr = [ 77.40, 0.00, 342.30 ];
	//$vpd = 164.13;




	//$vpd = 500;
	//$vpt = [base_w - 40, -30, 20];
	//$vpr = [140, 100, -20];
}

ShowPreview();

//
//
//// Set Viewport to show item we are currently debugging
//$vpt = [ -160.57, -63.78, 21.19 ];
//$vpr = [ 77.40, 0.00, 342.30 ];
//$vpd = 164.13;





module SampleOfHolesForBrassScrewInserts()
{
	ThreadedInsertSize = ThreadedInsertSize_M3_H4;
	TemplateWidth = BrassScrewInsertOuterDiameter(ThreadedInsertSize) + 25;
	TemplateLength = BrassScrewInsertOuterDiameter(ThreadedInsertSize) + 25;;
	TemplateHeight = BrassScrewInsertHeight(ThreadedInsertSize) + 3;
	difference()
	{
		#color("blue", 0.3)chamferCube(
			[TemplateWidth, TemplateLength, TemplateHeight],
			ch = 0.3,
			// chamfer top and sides to reduce sharp feal
			chamfers = [[1, 1, 1, 1],
						[1, 1, 1, 1],
						[1, 1, 1, 1]]);

		//WallMountScrews();

		// Center in Template and make holes for inserts

		translate([TemplateWidth/5, TemplateLength/5, TemplateHeight + ExtraHeightToMakeHolesGoThrough - BrassScrewInsertHeight(ThreadedInsertSize)])
		{
			BrassScrewInsert();


		}

		translate([TemplateWidth/3 - BrassScrewInsertOuterDiameter(ThreadedInsertSize_M2_H4),
				TemplateLength/3 -- BrassScrewInsertOuterDiameter(ThreadedInsertSize_M2_H4),
				 TemplateHeight + ExtraHeightToMakeHolesGoThrough - BrassScrewInsertHeight(ThreadedInsertSize_M2_H4)])
		{
			BrassScrewInsert(ThreadedInsertSize_M2_H4);


		}

		translate([TemplateWidth/2, TemplateLength/2, TemplateHeight + ExtraHeightToMakeHolesGoThrough - BrassScrewInsertHeight(ThreadedInsertSize_M3_H6)])
		{
			BrassScrewInsert(ThreadedInsertSize_M3_H6);


		}

		translate([TemplateWidth/2 + BrassScrewInsertOuterDiameter(ThreadedInsertSize_M4_H6),
				 TemplateLength/2 + BrassScrewInsertOuterDiameter(ThreadedInsertSize_M4_H6),
				  TemplateHeight + ExtraHeightToMakeHolesGoThrough - BrassScrewInsertHeight(ThreadedInsertSize_M4_H6)])
		{
			BrassScrewInsert(ThreadedInsertSize_M4_H6);


		}

		translate([TemplateWidth/2 + BrassScrewInsertOuterDiameter(ThreadedInsertSize_M4_H8),
				 TemplateLength/2 - BrassScrewInsertOuterDiameter(ThreadedInsertSize_M4_H8),
				 TemplateHeight + ExtraHeightToMakeHolesGoThrough - BrassScrewInsertHeight(ThreadedInsertSize_M4_H8)])
		{
			BrassScrewInsert(ThreadedInsertSize_M4_H8);


		}

		translate([TemplateWidth/2 - BrassScrewInsertOuterDiameter(ThreadedInsertSize_M5_H8),
				 TemplateLength/2 + BrassScrewInsertOuterDiameter(ThreadedInsertSize_M5_H8),
				 TemplateHeight + ExtraHeightToMakeHolesGoThrough - BrassScrewInsertHeight(ThreadedInsertSize_M5_H8)])
		{
			BrassScrewInsert(ThreadedInsertSize_M5_H8);


		}

		translate([TemplateWidth - 2*BrassScrewInsertOuterDiameter(ThreadedInsertSize_M6_H8),
				 TemplateLength/2 - BrassScrewInsertOuterDiameter(ThreadedInsertSize_M6_H8),
				 TemplateHeight + ExtraHeightToMakeHolesGoThrough - BrassScrewInsertHeight(ThreadedInsertSize_M6_H8)])
		{
			BrassScrewInsert(ThreadedInsertSize_M6_H8);


		}
	}
}

module TemplateForMarkingWallMountingSrewLocations()
{
	WallMounting_CoverPlate_For_Covering_Base(TemplateThickness = 3.5, UseTemplateMode = true, RemoveChunksForFasterPrototypePrinting = RemoveChunksForFasterPrototypePrinting);
}



module BrassScrewInserts(
	ThreadedInsertSize = ThreadedInsertSize_M3_H4,
	ScrewPatternWidthOfScrewCenters = WallMounting_ScrewInsertsPattern_WidthOfScrewCenters,
	ScrewPatternHeightOfScrewCenters = WallMounting_ScrewInsertsPattern_HeightOfScrewCenters,
	UseTheScrewsThatWillScrewIntoTheInserts = false
)
{
		//color("Red", 0.85)
		{
			// TopLeft Screw Hole
			if(UseTheScrewsThatWillScrewIntoTheInserts)
			{
				translate([0, ScrewPatternHeightOfScrewCenters, 0])
				{				
					rotate([0,180,0])
					{
						BoltHole_Metric(
						DrillTheCountersink = true,
						MetricScrewSize = MetricScrewSize,
						TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
							ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
							NumberOfSides = Resolution);
					}
				}
			}
			else
			{
				translate([0, ScrewPatternHeightOfScrewCenters, -BrassScrewInsertHeight(ThreadedInsertSize)])
				{				
					BrassScrewInsert(ThreadedInsertSize);
				}
			}
			
			// TopRight Screw Hole
			if(UseTheScrewsThatWillScrewIntoTheInserts)
			{
				translate([ScrewPatternWidthOfScrewCenters, ScrewPatternHeightOfScrewCenters, 0])
				{				
					rotate([0,180,0])
					{
						BoltHole_Metric(
						DrillTheCountersink = true,
						MetricScrewSize = MetricScrewSize,
						TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
							ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
							NumberOfSides = Resolution);
					}
				}
			}
			else
			{
				translate([ScrewPatternWidthOfScrewCenters, ScrewPatternHeightOfScrewCenters, -BrassScrewInsertHeight(ThreadedInsertSize)])
				{				
					BrassScrewInsert(ThreadedInsertSize);
				}
			}
			
			// BottomLeft Screw Hole
			//translate([0, 0, -BrassScrewInsertHeight(ThreadedInsertSize)])
			if(UseTheScrewsThatWillScrewIntoTheInserts)
			{
				translate([0, 0, 0])
				{				
					rotate([0,180,0])
					{
						BoltHole_Metric(
						DrillTheCountersink = true,
						MetricScrewSize = MetricScrewSize,
						TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
							ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
							NumberOfSides = Resolution);
					}
				}
			}
			else
			{
				translate([0, 0, -BrassScrewInsertHeight(ThreadedInsertSize)])
				{				
					BrassScrewInsert(ThreadedInsertSize);
				}
			}
			
			// BottomRight Screw Hole
			//translate([ScrewPatternWidthOfScrewCenters, 0, -BrassScrewInsertHeight(ThreadedInsertSize)])
			if(UseTheScrewsThatWillScrewIntoTheInserts)
			{
				translate([ScrewPatternWidthOfScrewCenters, 0, 0])
				{				
					rotate([0,180,0])
					{
						BoltHole_Metric(
						DrillTheCountersink = true,
						MetricScrewSize = MetricScrewSize,
						TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
							ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
							NumberOfSides = Resolution);
					}
				}
			}
			else
			{
				translate([ScrewPatternWidthOfScrewCenters, 0, -BrassScrewInsertHeight(ThreadedInsertSize)])
				{				
					BrassScrewInsert(ThreadedInsertSize);
				}
			}
		}		
}






module WallMountScrews(
	ScrewPatternWidthOfScrewCenters = WallMounting_ScrewPattern_WidthOfScrewCenters,
	ScrewPatternHeightOfScrewCenters = WallMounting_ScrewPattern_HeightOfScrewCenters,
	MetricScrewSize = MetricScrewSize
)
{

		// Drill Holes to mount box to wall
		//color("Red", 0.85)
		{


			// TopLeft Screw Hole
			translate([0, ScrewPatternHeightOfScrewCenters, 0])
			{
				rotate([0,180,0])
				{
					BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
					ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
						 NumberOfSides = Resolution);
				}
			}
			
			// TopRight Screw Hole
			translate([ScrewPatternWidthOfScrewCenters, ScrewPatternHeightOfScrewCenters, 0])
			{
				rotate([0,180,0])
				{
					BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
					ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
						 NumberOfSides = Resolution);
				}
			}
			
			// BottomLeft Screw Hole
			translate([0, 0, 0])
			{
				rotate([0,180,0])
				{
					BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
					ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
						 NumberOfSides = Resolution);
				}
			}
			
			// BottomRight Screw Hole
			translate([ScrewPatternWidthOfScrewCenters, 0, 0])
			{
				rotate([0,180,0])
				{
					BoltHole_Metric(
					DrillTheCountersink = true,
					MetricScrewSize = MetricScrewSize,
					TotalBoltLength=BoxDepth + 2*ExtraHeightToMakeHolesGoThrough,
					ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls = 
						ExtraRadiusSoScrewsSlipRightThroughWithoutThreadsEngagingHoleWalls,
						 NumberOfSides = Resolution);
				}
			}
		}		
}



module WallMounting_Base(
	TemplateThickness = WallMounting_Base_Thickness,
	WallMounting_GapBetweenBaseAndPlate = 0, // default to normal size with no extra size.  plate will pass in gap size to remove a slightly bloated base
	Drill_WallMountScrews_Holes = true, // don't wan't screw holes removed when plate is removing base or it will leave material behind where the hole was
	Drill_BrassScrewInserts_Holes = true, // don't wan't insert holes removed when plate is removing base or it will leave material behind where the hole was
	RemoveChunksForFasterPrototypePrinting = false,
	CutOutChannelForWires = true,
	SkipAllChamfers = SkipAllChamfers)
{
	ThreadedInsertSize = ThreadedInsertSize_M3_H4;
	TemplateWidth = WallMounting_Base_Width + WallMounting_GapBetweenBaseAndPlate;
	TemplateHeight = WallMounting_Base_Height + 2*WallMounting_GapBetweenBaseAndPlate;

	
	// Either skip all Chamfers or use custom chamfer settings
	ChamferSelection = SkipAllChamfers
		? 
			[[0, 0, 0, 0],
			[0, 0, 0, 0],
			[0, 0, 0, 0]]
		:
			[[1, 1, 1, 1],
			[1, 1, 1, 1],
			[1, 1, 1, 1]]
		;


	difference()
	{
		color("Green", 0.2)
		chamferCube(
			[TemplateWidth, TemplateHeight, TemplateThickness],
			ch = 0.5,
			// chamfer top and sides to reduce sharp feal
			chamfers = ChamferSelection);

		if(Drill_WallMountScrews_Holes)
		{
			// Position in template and Make holes for wall mount screws (where we screw mounting plate to wall)
			translate(	[WallMounting_ScrewPattern_OffsetFromLeft,
						TemplateHeight/2 - WallMounting_ScrewPattern_HeightOfScrewCenters/2,
						TemplateThickness + ExtraHeightToMakeHolesGoThrough])
			{
				WallMountScrews();
			}
		}

		if(Drill_BrassScrewInserts_Holes)
		{
			// Position in template and Make holes for threaded screw inserts (where we screw parts onto mounting plate)
			translate(	[WallMounting_ScrewInsertsPattern_OffsetFromLeft,
						TemplateHeight/2 - WallMounting_ScrewInsertsPattern_HeightOfScrewCenters/2,
						TemplateThickness + ExtraHeightToMakeHolesGoThrough])
			{
				BrassScrewInserts(ThreadedInsertSize);
			}
		}

		// Cut a channel out of the back of the base for wires to be run behind/through it.
		if(CutOutChannelForWires)
		{
			color("Orange", 0.85)
			{
				translate([WallMounting_Base_Channel_WidthOfWallToLeave,TemplateHeight/2 - WallMounting_Base_Channel_Height/2,0 - ExtraHeightToMakeHolesGoThrough])
				{
					cube([WallMounting_Base_Channel_Width, WallMounting_Base_Channel_Height, WallMounting_Base_Channel_Thickness + ExtraHeightToMakeHolesGoThrough], center=false);
				}
			}
		}

		// If we are printing a template for positioning markes for drilling holes in wall for screws, remove large chunks from print job to speeed up the print
		if(RemoveChunksForFasterPrototypePrinting)
		{
			BorderToLeaveHeight = 5;

			//first block
			translate([5, BorderToLeaveHeight, -ExtraHeightToMakeHolesGoThrough])
			{
				chamferCube(
					[22, TemplateHeight - 2*BorderToLeaveHeight, TemplateThickness + 2*ExtraHeightToMakeHolesGoThrough],
					ch = 0.5,
					// chamfer top and sides to reduce sharp feal
					chamfers = ChamferSelection);
			}

			//second block
			translate([38, BorderToLeaveHeight, -ExtraHeightToMakeHolesGoThrough])
			{
				chamferCube(
					[30, TemplateHeight - 2*BorderToLeaveHeight, TemplateThickness + 2*ExtraHeightToMakeHolesGoThrough],
					ch = 0.5,
					// chamfer top and sides to reduce sharp feal
					chamfers = ChamferSelection);
			}

			//third block
			translate([76, BorderToLeaveHeight, -WallMounting_CoverPlate_CornerHook_Thickness -ExtraHeightToMakeHolesGoThrough])
			{
				chamferCube(
					[15, TemplateHeight - 2*BorderToLeaveHeight, TemplateThickness + WallMounting_CoverPlate_CornerHook_Thickness + 2*ExtraHeightToMakeHolesGoThrough],
					ch = 0.5,
					// chamfer top and sides to reduce sharp feal
					chamfers = ChamferSelection);
			}
		}
	}
}

module WallMounting_CoverPlate_For_Covering_Base(
	TemplateThickness = WallMounting_CoverPlate_Thickness,
	UseTemplateMode = false,
	RemoveChunksForFasterPrototypePrinting = false,
	SkipAllChamfers = SkipAllChamfers)
{
	ThreadedInsertSize = ThreadedInsertSize_M3_H4;
	TemplateWidth = WallMounting_CoverPlate_Width;
	TemplateHeight = WallMounting_CoverPlate_Height;

	
	// Either skip all Chamfers or use custom chamfer settings
	ChamferSelection = SkipAllChamfers
		? 
			[[0, 0, 0, 0],
			[0, 0, 0, 0],
			[0, 0, 0, 0]]
		:
			[[1, 1, 1, 1],
			[1, 1, 1, 1],
			[1, 1, 1, 1]]
		;

	
	// Either skip all Chamfers or use custom chamfer settings
	ChamferSelectionForBaseCover = SkipAllChamfers
		? 
			[[0, 0, 0, 0],
			[0, 0, 0, 0],
			[0, 0, 0, 0]]
		:
			[[1, 1, 1, 1],
			[1, 1, 0, 0], // Y-axis.  Looking from 0,0 towards the negative X
			[1, 0, 0, 1]] // Z-axis.  Looling down, starting at bottom left, moving counter clockwize
		;


	
	// Either skip all Chamfers or use custom chamfer settings
	ChamferSelectionForBaseCoverHook = SkipAllChamfers
		? 
			[[0, 0, 0, 0],
			[0, 0, 0, 0],
			[0, 0, 0, 0]]
		:
			[[1, 1, 1, 1],
			[1, 0, 0, 1],
			[0, 1, 1, 0]]
		;


	difference()
	{
		union()
		{
			//color("blue", 0.3)
			chamferCube(
				[TemplateWidth, TemplateHeight, TemplateThickness],
				ch = 0.5,
				// chamfer top and sides to reduce sharp feel
				chamfers = ChamferSelectionForBaseCover);

			if(WallMounting_CoverPlate_CornerHook_AddToModel)
			{
				translate([TemplateWidth, 0, -WallMounting_CoverPlate_CornerHook_Thickness])
				{
					chamferCube(
						[WallMounting_CoverPlate_CornerHook_Width, TemplateHeight, WallMounting_CoverPlate_CornerHook_Thickness + TemplateThickness],
						ch = 0.5,
						// chamfer top and sides to reduce sharp feel	1, 1, 1, 1	0, 0, 0, 0
						chamfers = ChamferSelectionForBaseCoverHook);
				}
			}
		}

		BaseRemovalDepth = UseTemplateMode
			? TemplateThickness-ExtraHeightToMakeHolesGoThrough
			: -ExtraHeightToMakeHolesGoThrough
			;
		// echo(TemplateThickness = TemplateThickness);
		// echo(BaseRemovalDepth = BaseRemovalDepth);
		
		

		// remove base
		translate([
			WallMounting_CoverPlate_BorderAroundBase - WallMounting_GapBetweenBaseAndPlate,
			WallMounting_CoverPlate_BorderAroundBase - WallMounting_GapBetweenBaseAndPlate,
			BaseRemovalDepth])
		{
			//color("blue", 0.5)
			{
				WallMounting_Base(
					RemoveChunksForFasterPrototypePrinting = false,
					WallMounting_GapBetweenBaseAndPlate = WallMounting_GapBetweenBaseAndPlate,
					Drill_WallMountScrews_Holes = false,
					Drill_BrassScrewInserts_Holes = false,
					CutOutChannelForWires = false);
			}
		}

		

		RemoveChannelToCoverPlateBox = true;
		if(RemoveChannelToCoverPlateBox)
		{
			translate([
				WallMounting_CoverPlate_BorderAroundBase + WallMounting_Base_Width - ExtraHeightToMakeHolesGoThrough,
				WallMounting_CoverPlate_BorderAroundBase - WallMounting_GapBetweenBaseAndPlate,
				BaseRemovalDepth])
			{
				cube([
					TemplateWidth - WallMounting_CoverPlate_BorderAroundBase - WallMounting_Base_Width,
					WallMounting_Base_Height,
					WallMounting_Base_Thickness]);
					
				translate([
					22,
					20,
					BaseRemovalDepth])
				{
					cube([
						WallMounting_CoverPlate_Box_Thickness/2,
						WallMounting_Base_Height*2/3,
						WallMounting_Base_Thickness + 20]);
				}
			}
			
		}

		if(UseTemplateMode)
		{
			// we only need the wood screw holes that mount to the wall for making drill guide in the template
			// Position in template and Make holes for wall mount screws (where we screw mounting plate to wall)
			translate(	[WallMounting_ScrewPattern_OffsetFromLeft + WallMounting_CoverPlate_BorderAroundBase + WallMounting_GapBetweenBaseAndPlate,
						TemplateHeight/2 - WallMounting_ScrewPattern_HeightOfScrewCenters/2,
						TemplateThickness + ExtraHeightToMakeHolesGoThrough])
			{
				WallMountScrews(MetricScrewSize=MetricScrewSize_ForTemplateWoodScrewHoles);
			}
		}

		// Position in template and Make holes for screws that screw into the threaded screw inserts (where we screw parts onto mounting plate)
		translate(	[WallMounting_ScrewPattern_OffsetFromLeft + WallMounting_CoverPlate_BorderAroundBase + WallMounting_GapBetweenBaseAndPlate,
					TemplateHeight/2 - WallMounting_ScrewInsertsPattern_HeightOfScrewCenters/2,
					TemplateThickness + ExtraHeightToMakeHolesGoThrough])
		{
			BrassScrewInserts(UseTheScrewsThatWillScrewIntoTheInserts = true);
		}

		// If we are printing a template for positioning markes for drilling holes in wall for screws, remove large chunks from print job to speeed up the print
		if(RemoveChunksForFasterPrototypePrinting)
		{
			BorderToLeaveHeight = 6.5;

			//first block
			translate([5, BorderToLeaveHeight, -ExtraHeightToMakeHolesGoThrough])
			{
				chamferCube(
					[27, TemplateHeight - 2*BorderToLeaveHeight, TemplateThickness + 2*ExtraHeightToMakeHolesGoThrough],
					ch = 0.5,
					// chamfer top and sides to reduce sharp feal
					chamfers = ChamferSelection);
			}

			//second block
			translate([44, BorderToLeaveHeight, -ExtraHeightToMakeHolesGoThrough])
			{
				chamferCube(
					[27, TemplateHeight - 2*BorderToLeaveHeight, TemplateThickness + 2*ExtraHeightToMakeHolesGoThrough],
					ch = 0.5,
					// chamfer top and sides to reduce sharp feal
					chamfers = ChamferSelection);
			}

			//third block
			translate([83, BorderToLeaveHeight, -WallMounting_CoverPlate_CornerHook_Thickness -ExtraHeightToMakeHolesGoThrough])
			{
				chamferCube(
					[78, TemplateHeight - 2*BorderToLeaveHeight, TemplateThickness + WallMounting_CoverPlate_CornerHook_Thickness + 2*ExtraHeightToMakeHolesGoThrough],
					ch = 0.5,
					// chamfer top and sides to reduce sharp feal
					chamfers = ChamferSelection);
			}
		}
	}
}