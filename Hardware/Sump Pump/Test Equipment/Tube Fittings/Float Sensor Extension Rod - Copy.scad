// Extension Rod(s) for Sump Pump Ranger by Your Smart Solutions

use <threads_v2p1.scad>
//MetricNut(7);




// Internal threads on the bottom, external threads on the top.
module RodExtender(diameter, height, thread_len=0, thread_diam=0, thread_pitch=0) {
  // A reasonable default.
  thread_diam = (thread_diam==0) ? 0.75*diameter : thread_diam;
  thread_len = (thread_len==0) ? 0.5*diameter : thread_len;
  thread_pitch = (thread_pitch==0) ? ThreadPitch(thread_diam) : thread_pitch;
  
  max_bridge = height - thread_len;
  // Use 60 degree slope if it will fit.
  bridge_height = ((thread_diam/4) < max_bridge) ? thread_diam/4 : max_bridge;

  difference() {
    union() {
      ScrewHole(thread_diam, thread_len, pitch=thread_pitch)
        cylinder(r=diameter/2, h=height, $fn=24*diameter);
  
      translate([0,0,height])
        ScrewThread(thread_diam, thread_len, pitch=thread_pitch,
          tip_height=thread_pitch, tip_min_fract=0.75);
    }
    // Carve out a small conical area as a bridge.
    translate([0,0,thread_len])
      cylinder(h=bridge_height, r1=thread_diam/2, r2=0.1);
  }
}



include <YourSmartSolutions_Constants.scad>
use <YourSmartSolutions_Functions.scad>


//MetricBoltSet(7, 20);  // The second number is the nominal height of the bolt.

RodExtenderOuterDiameter = 10;
RodExtenderHeight = 20;
GapBetweenParts = 4;

//translate([0*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
//{
//	difference()
//	{
//		RodExtender(RodExtenderOuterDiameter,RodExtenderHeight);
//		cylinder(d=4, RodExtenderHeight + 5 + ExtraHeightToMakeHolesGoThrough, $fn=16);
//	}
//}
	
translate([1*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
{
	difference()
	{
		RodExtender(RodExtenderOuterDiameter, (RodExtenderHeight * 2));
		cylinder(d=4, (RodExtenderHeight * 2) + 5 + ExtraHeightToMakeHolesGoThrough, $fn=16);
	}
}
	
	
translate([2*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
{
	difference()
	{
        RodExtender(RodExtenderOuterDiameter,RodExtenderHeight * 4);
		cylinder(d=4, (RodExtenderHeight * 4) + 5 + ExtraHeightToMakeHolesGoThrough, $fn=16);
	}
}
	
	
//translate([3*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
//{
//	difference()
//	{
//        RodExtender(RodExtenderOuterDiameter,RodExtenderHeight * 4);
//		cylinder(d=4, (RodExtenderHeight * 4) + 5 + ExtraHeightToMakeHolesGoThrough, $fn=16);
//	}
//}
	
//	
//translate([4*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
//{
//	difference()
//	{
//        RodExtender(RodExtenderOuterDiameter,RodExtenderHeight * 7);
//		cylinder(d=4, (RodExtenderHeight * 7) + 5 + ExtraHeightToMakeHolesGoThrough, $fn=16);
//	}
//}
	
//	
//translate([5*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
//{
//	difference()
//	{
//        RodExtender(RodExtenderOuterDiameter,RodExtenderHeight * 8.5);
//		cylinder(d=4, (RodExtenderHeight * 8.5) + 5 + ExtraHeightToMakeHolesGoThrough, $fn=16);
//	}
//}
	
//translate([3*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
//	RodExtender(RodExtenderOuterDiameter,RodExtenderHeight * 7);
