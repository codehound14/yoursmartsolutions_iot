// Extension Rod(s) for Sump Pump Ranger by Your Smart Solutions

use <threads_v2p1.scad>
//MetricNut(8);






include <YourSmartSolutions_Constants.scad>
use <YourSmartSolutions_Functions.scad>


//MetricBoltSet(7, 20);  // The second number is the nominal height of the bolt.

$fn=64;

RodExtenderOuterDiameter = 10;
RodExtenderHeight = 15;
RodExtenderThreadDiameter = 8; // [4, 6, 8, 10]
RodExtenderThreadLength = 5;
DiameterOfHoleInExtenderForWires = 4;
GapBetweenParts = 4;



//translate([0,-20,0])
//{
//	MetricNut(RodExtenderThreadDiameter);
//	//MetricBolt();
//	//MetricBoltSet(diameter = RodExtenderHeightThreadDiameter, length = RodExtenderThreadLength);
//}

//translate([RodExtenderThreadDiameter*2,-20,0])
//{
//	MetricNut(RodExtenderThreadDiameter);
//	//MetricBolt();
//	//MetricBoltSet(diameter = RodExtenderHeightThreadDiameter, length = RodExtenderThreadLength);
//}

//translate([RodExtenderThreadDiameter*4,-20,0])
//{
//	MetricNut(RodExtenderThreadDiameter);
//	//MetricBolt();
//	//MetricBoltSet(diameter = RodExtenderHeightThreadDiameter, length = RodExtenderThreadLength);
//}
//
//translate([RodExtenderThreadDiameter*6,-20,0])
//{
//	MetricNut(RodExtenderThreadDiameter);
//	//MetricBolt();
//	//MetricBoltSet(diameter = RodExtenderHeightThreadDiameter, length = RodExtenderThreadLength);
//}



translate([0*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
{
	difference()
	{
		RodExtender(diameter = RodExtenderOuterDiameter, height = RodExtenderHeight, thread_len = RodExtenderThreadLength, thread_diam = RodExtenderThreadDiameter);
		cylinder(d=DiameterOfHoleInExtenderForWires, RodExtenderHeight + RodExtenderThreadLength + ExtraHeightToMakeHolesGoThrough);
	}
}



translate([-1*(RodExtenderOuterDiameter+GapBetweenParts),1*(RodExtenderOuterDiameter+GapBetweenParts),0])
{
	difference()
	{
		RodExtender(diameter = RodExtenderOuterDiameter, height = (RodExtenderHeight*2), thread_len = RodExtenderThreadLength, thread_diam = RodExtenderThreadDiameter);
		cylinder(d=DiameterOfHoleInExtenderForWires, (RodExtenderHeight*2) + RodExtenderThreadLength + ExtraHeightToMakeHolesGoThrough);
	}
}


//translate([-2*(RodExtenderOuterDiameter+GapBetweenParts),2*(RodExtenderOuterDiameter+GapBetweenParts),0])
//{
//	difference()
//	{
//		RodExtender(diameter = RodExtenderOuterDiameter, height = (RodExtenderHeight*3), thread_len = RodExtenderThreadLength, thread_diam = RodExtenderThreadDiameter);
//		cylinder(d=DiameterOfHoleInExtenderForWires, (RodExtenderHeight*3) + RodExtenderThreadLength + ExtraHeightToMakeHolesGoThrough);
//	}
//}
//
//
//translate([-3*(RodExtenderOuterDiameter+GapBetweenParts),3*(RodExtenderOuterDiameter+GapBetweenParts),0])
//{
//	difference()
//	{
//		RodExtender(diameter = RodExtenderOuterDiameter, height = (RodExtenderHeight*4), thread_len = RodExtenderThreadLength, thread_diam = RodExtenderThreadDiameter);
//		cylinder(d=DiameterOfHoleInExtenderForWires, (RodExtenderHeight*4) + RodExtenderThreadLength + ExtraHeightToMakeHolesGoThrough);
//	}
//}
//
//
//translate([-4*(RodExtenderOuterDiameter+GapBetweenParts),4*(RodExtenderOuterDiameter+GapBetweenParts),0])
//{
//	difference()
//	{
//		RodExtender(diameter = RodExtenderOuterDiameter, height = (RodExtenderHeight*5), thread_len = RodExtenderThreadLength, thread_diam = RodExtenderThreadDiameter);
//		cylinder(d=DiameterOfHoleInExtenderForWires, (RodExtenderHeight*5) + RodExtenderThreadLength + ExtraHeightToMakeHolesGoThrough);
//	}
//}
	
//translate([1*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
//{
//	difference()
//	{
//		RodExtender(RodExtenderOuterDiameter, (RodExtenderHeight * 2));
//		cylinder(d=4, (RodExtenderHeight * 2) + 5 + ExtraHeightToMakeHolesGoThrough, $fn=16);
//	}
//}
	
	
//translate([2*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
//{
//	difference()
//	{
//        RodExtender(RodExtenderOuterDiameter,RodExtenderHeight * 4);
//		cylinder(d=4, (RodExtenderHeight * 4) + 5 + ExtraHeightToMakeHolesGoThrough, $fn=16);
//	}
//}
	
	
//translate([3*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
//{
//	difference()
//	{
//        RodExtender(RodExtenderOuterDiameter,RodExtenderHeight * 4);
//		cylinder(d=4, (RodExtenderHeight * 4) + 5 + ExtraHeightToMakeHolesGoThrough, $fn=16);
//	}
//}
	
//	
//translate([4*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
//{
//	difference()
//	{
//        RodExtender(RodExtenderOuterDiameter,RodExtenderHeight * 7);
//		cylinder(d=4, (RodExtenderHeight * 7) + 5 + ExtraHeightToMakeHolesGoThrough, $fn=16);
//	}
//}
	
//	
//translate([5*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
//{
//	difference()
//	{
//        RodExtender(RodExtenderOuterDiameter,RodExtenderHeight * 8.5);
//		cylinder(d=4, (RodExtenderHeight * 8.5) + 5 + ExtraHeightToMakeHolesGoThrough, $fn=16);
//	}
//}
	
//translate([3*(RodExtenderOuterDiameter+GapBetweenParts),0,0])
//	RodExtender(RodExtenderOuterDiameter,RodExtenderHeight * 7);
