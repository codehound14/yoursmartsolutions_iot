// End Cap for Sump Pump Ranger test rig
// codehound December 2020

Layout = "Build";

$fn = 64;

// Print a rim at the top of the cap
PrintCapRim = true;

//- Extrusion parameters - must match reality!
ThreadThick = 0.25;
ThreadWidth = 1.50;

function IntegerMultiple(Size,Unit) = Unit * ceil(Size / Unit);

Protrusion = 0.1;

HoleWindage = 0.2;

//- Screw sizes

MillimetersPerInch = 25.4;

// enter inner diameter of tube cap in inches or millimeters
TubeID = 3.12 * MillimetersPerInch;

TubeWall = 0.1 * MillimetersPerInch;

CapInsertDepth = 15.0;
CapInsertDepth = 2.0;

CapRimThickness = 0.8;
CapWall = 1*ThreadWidth;


PrintFlanges = true;
NumFlanges = 1;
FlangeHeight = 3*ThreadThick;
FlangeWidth = ThreadWidth/2;
FlangeSpace = CapInsertDepth / (NumFlanges + 1);

OAHeight = CapInsertDepth + CapRimThickness;
//OAHeight = 14;

PrintReinforcementFins = true;
NumberOfReenforcementFins = 2;
NumberOfSides = 64; // resolution for circles

FloatSensorDiameter = 25;
FloatSensorScrewHoleDiameter = 10.5;

WaterTubeOuterDiameter = 14;

CutHoleForFullSensor = true;
// X,Y,Height, Countersink Pitch
FullSensorHoleLocation = [12,16,18,4];

//- Adjust hole diameter to make the size come out right

module PolyCyl(Dia,Height,ForceSides=0) {			// based on nophead's polyholes

  Sides = (ForceSides != 0) ? ForceSides : (ceil(Dia) + 2);

  FixDia = Dia / cos(180/Sides);

  cylinder(r=(FixDia + HoleWindage)/2,h=Height,$fn=Sides);
}

module TubeCap() {

  difference() {
      // main cylinder
    cylinder(d=TubeID,h=OAHeight,$fn=NumberOfSides);
      
      // carve out inside of cap
    translate([0,0,CapWall])
      cylinder(d=TubeID - 2*CapWall,h=OAHeight,$fn=NumberOfSides);
      
      // cut hole for full sensor (drop through)
      if(CutHoleForFullSensor)
      {
        translate([15,15,-20])
          cylinder(d=FloatSensorDiameter, h=50);
      }
      
    translate([-18,18,-20])
      cylinder(d=FloatSensorScrewHoleDiameter, h=50);
    translate([-18.5,-18.5,-20])
      cylinder(d=FloatSensorScrewHoleDiameter, h=50);
    translate([10,-10,-20])
      cylinder(d=FloatSensorScrewHoleDiameter, h=50);
      
      // cut a water tube hole
    translate([20,-20,-30])
      cylinder(d=WaterTubeOuterDiameter, h=80);
  }

	if(PrintFlanges)
	{	
      for (i=[1:NumFlanges])
        translate([0,0,i*FlangeSpace])
          difference() {
            cylinder(d=TubeID + 2*FlangeWidth,h=FlangeHeight,$fn=NumberOfSides);
            translate([0,0,-Protrusion])
              cylinder(d=TubeID - 2*CapWall,h=FlangeHeight + 2*Protrusion,$fn=NumberOfSides);
          }
	}

    
    if(PrintReinforcementFins)
    {
      for (i=[0:NumberOfReenforcementFins-1])
        rotate(i*360/NumberOfReenforcementFins)
          translate([0,-ThreadWidth,CapWall + ThreadThick])
            cube([TubeID/2 - CapWall/2,2*ThreadWidth,CapInsertDepth + CapRimThickness - CapWall - ThreadThick],center=false);
    }

	if(PrintCapRim)
	{	
	  translate([0,0,CapInsertDepth]) {
		difference() {
		  cylinder(d=TubeID + 2*TubeWall,h=CapRimThickness,$fn=NumberOfSides);
		  translate([0,0,-Protrusion])
			cylinder(d=TubeID - 3*2*CapWall,h=2*CapRimThickness,$fn=NumberOfSides);
		}
	  }
	}

//  translate([5,10,20]) {
//    difference() {
//      cylinder(d=25, h=50);
//    }
//  }
}

//- Build things

if (Layout == "Show")
  TubeCap();

if (Layout == "Build")
  translate([0,0,OAHeight])
    rotate([180,0,0])
      TubeCap();