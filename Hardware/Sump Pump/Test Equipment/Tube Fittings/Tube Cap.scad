// End Cap for Sump Pump Ranger test rig
// codehound December 2020

// Reference to section of manual for parameter definition
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Customizer#hidden_parameters

// Threads and screws and bolts library
//https://www.thingiverse.com/thing:1686322
use <threads_v2p1.scad>



//RodExtender(5,3);

/* [Global] */ // These show up in every tab
//ApplicationName = "End Cap for Sump Pump Ranger";

/* [Hidden] */  //These don't not show up at all in the customizer
ApplicationVersion = "0.4";

/* [General] */
Layout = "Normal"; // [Normal, Inverted]

// Resolution of circles
NumberOfSides = 256; // [10:Low, 64:Normal, 256:High]

ExtraHeightToMakeHolesGoThrough = 1.0;

// Resolution of circles
CapTopThickness = 1; // [1:Thin, 3:Normal, 7:Thick]

// Print Name and Version Number
InfoBlock_1_Print = true;
InfoBlock_1_X = -30.0;
InfoBlock_1_Y = -23.5;
InfoBlock_1_Z = 25;
InfoBlock_1_ExtrudeHeight = 1.0;
InfoBlock_1_Size = 7;
InfoBlock_1_Text = "Tube Cap 1.2";

/* [Rim Cap:] */
// Print a rim at the top of the cap
PrintCapRim = true;
CapRimThickness = 1.0;
CapRimOverhang = 1.5;


function IntegerMultiple(Size,Unit) = Unit * ceil(Size / Unit);

Protrusion = 0.1;

HoleWindage = 0.2;

//- Screw sizes

MillimetersPerInch = 25.4;

// enter inner diameter of tube cap in inches or millimeters
//TubeInnerDiameter = 3.12 * MillimetersPerInch;
// 79.248 = 3.12 inches * 25.4 mm/inch which is close but just a little too tight to fit without a taper
TubeInnerDiameter = 78.75; // we'll try this one as our first try after switching to metric

// Thickness of main cap vertical wall
CapVerticalWallThickness = 2.0;

// Height of main cap vertical wall
CapVerticalWallHeight = 4.0;

CapTaperWallHeight = 6.0;
CapTaperWallDiameterDelta = 0.75;


TubeWall = 0.1 * MillimetersPerInch;

CapInsertDepth = 2.0;


/* [Sensor Details:] */
FloatSensorDiameter = 25;
FloatSensorScrewHoleDiameter = 10.5;
FloatSensorLabelText = "Float";

// Outer diameter of water tube = inner diameter of hole
WaterTubeOuterDiameter = 13;

CutHoleForSensorThreads = true;

CutHoleForSensorExtensionRodHole = true;
ExtensionRodHoleDiameter = 8.5;

CutHoleForFullSensor = false;
// X,Y,Height, Countersink Pitch
FullSensorHoleLocation = [12,16,18,4];

/* [Sensor: Threaded Extension Pipe] */
PrintSensorExtensionPipe = true;



/* [Sensor Holes] */
Sensor_1_WaterPipeHole_Print = false;
Sensor_1_WaterPipeHole_Location_X = 25;
Sensor_1_WaterPipeHole_Location_Y = 0;
Sensor_1_WaterPipeHole_Location_Z = -1.5;
Sensor_1_WaterPipeHole_Height = 5.0;
Sensor_1_WaterPipeHole_Diameter = WaterTubeOuterDiameter;

Sensor_2_WaterPipeHole_Print = false;
Sensor_2_WaterPipeHole_Location_X = 0;
Sensor_2_WaterPipeHole_Location_Y = -25;
Sensor_2_WaterPipeHole_Location_Z = -1.5;
Sensor_2_WaterPipeHole_Height = 5.0;
Sensor_2_WaterPipeHole_Diameter = WaterTubeOuterDiameter;

Sensor_3_WaterPipeHole_Print = false;
Sensor_3_WaterPipeHole_Diameter = WaterTubeOuterDiameter;
Sensor_3_WaterPipeHole_Location_X = -25;
Sensor_3_WaterPipeHole_Location_Y = 0;
Sensor_3_WaterPipeHole_Location_Z = -1.5;
Sensor_3_WaterPipeHole_Height = 5.0;

PlugSocket_Print = true;
PlugSocket_Length = 27;
PlugSocket_Width = 20;
PlugSocket_Location_X = -10;
PlugSocket_Location_Y = 8;
PlugSocket_Location_Z = -1.5;
PlugSocket_Height = 45.0;


module PrintText(X = 0, Y = 0, Z = 60, LabelText = "Undefined Text", Size = 3, ExtrudeHeight = 2)
{    
    translate([X, Y, Z])
    {
        //linerar_extrude(ExtrudeHeight)
        linear_extrude(ExtrudeHeight)

        text(LabelText, size = Size);
    }
}




module BlankTubeCap() {

	// if rim is used and it is thicker, use its thickness
	TotalCapTopThickness = (PrintCapRim == true) ? max(CapTopThickness, CapRimThickness) : CapTopThickness;
	
	PrintTaperedVerticalWall = true;
	
	difference()
	{
		if(true)
		{
			if(PrintCapRim)
			{
				// rim of cap
				cylinder(d=TubeInnerDiameter + (2*CapRimOverhang), h=CapRimThickness, $fn=NumberOfSides);
			}
			
			// main cylinder which acts as base of cap (unless rim is thick enough)
			if(!PrintCapRim || (CapTopThickness > CapRimThickness))
			{
				cylinder(d=TubeInnerDiameter, h=CapTopThickness, $fn=NumberOfSides);
			}
			
			// core cylinder solid for vertical wall
			echo("max now =", max(CapTopThickness, CapRimThickness));
			echo("Using TotalCapTopThickness of ", TotalCapTopThickness);
			translate([0,0,TotalCapTopThickness])
			cylinder(d=TubeInnerDiameter, h=CapVerticalWallHeight, $fn=NumberOfSides);
			
			if(PrintTaperedVerticalWall)
			{
				// chamfered cylinder for tapered solid vertical wall extension (easier insertion)
				translate([0,0,TotalCapTopThickness + CapVerticalWallHeight])
				{
					//cylinder(d=TubeInnerDiameter, h=CapTaperWallHeight, $fn=NumberOfSides);
					cylinder(r1=TubeInnerDiameter/2, r2=TubeInnerDiameter/2 - CapTaperWallDiameterDelta, h=CapTaperWallHeight, $fn=NumberOfSides);
				}
			}
		
		}
			
		// remove inside of cylinder for vertical wall
		translate([0,0,TotalCapTopThickness])
		cylinder(d=TubeInnerDiameter - (2*CapVerticalWallThickness), h=CapVerticalWallHeight+ExtraHeightToMakeHolesGoThrough, $fn=NumberOfSides);
		
		if(PrintTaperedVerticalWall)
		{
			// remove inside of cylinder for tapered wall
			translate([0,0,TotalCapTopThickness + CapVerticalWallHeight])
			cylinder(d=TubeInnerDiameter - (2*CapVerticalWallThickness), h=CapVerticalWallHeight+ExtraHeightToMakeHolesGoThrough*3, $fn=NumberOfSides);
		}
		
	}
	
	if(InfoBlock_1_Print)
	{
		PrintText(X=InfoBlock_1_X, Y=InfoBlock_1_Y, Z=TotalCapTopThickness,
			LabelText=InfoBlock_1_Text,
			Size = InfoBlock_1_Size,
			ExtrudeHeight = InfoBlock_1_ExtrudeHeight);
	}
		
}


//- Build things

if (Layout == "Normal")
{
//	translate([250,250,-350])
//	{
//		TubeCap();
//	}
	translate([0,0,0])
	{
		difference()
		{
			BlankTubeCap();
			
			// Remove any shapes here
      
			// cut hole for full sensor (drop through)
		  if(CutHoleForFullSensor)
		  {
			translate([15,0,-20])
			  cylinder(d=FloatSensorDiameter, h=50);
			translate([-15,-10,-20])
			  cylinder(d=FloatSensorDiameter, h=50);
		  }
		  
			if(CutHoleForSensorThreads)
			{
				translate([0,0,-9])
					cylinder(d=FloatSensorScrewHoleDiameter, h=19);
			}
		  
			if(CutHoleForSensorExtensionRodHole)
			{
				translate([15, 0, -9])
					cylinder(d=ExtensionRodHoleDiameter, h=19);
				
				translate([-15,-10,-9])
					cylinder(d=ExtensionRodHoleDiameter, h=19);
			}
		  
		  
			
			
			  
		  if(PlugSocket_Print)
		  {
			  translate([PlugSocket_Location_X,PlugSocket_Location_Y,PlugSocket_Location_Z])
		  cube([PlugSocket_Width,PlugSocket_Length,18], center = false);
		  }
		  
		  
			
			
		  
		  if(Sensor_1_WaterPipeHole_Print)
		  {
			  translate([Sensor_1_WaterPipeHole_Location_X,Sensor_1_WaterPipeHole_Location_Y,Sensor_1_WaterPipeHole_Location_Z])
			  {
				cylinder(d=Sensor_1_WaterPipeHole_Diameter, h=Sensor_1_WaterPipeHole_Height);
			  }
		  }
			
		  if(Sensor_2_WaterPipeHole_Print)
		  {
			  translate([Sensor_2_WaterPipeHole_Location_X,Sensor_2_WaterPipeHole_Location_Y,Sensor_2_WaterPipeHole_Location_Z])
			  {
				cylinder(d=Sensor_2_WaterPipeHole_Diameter, h=Sensor_2_WaterPipeHole_Height);
			  }
		  }
			
		  if(Sensor_3_WaterPipeHole_Print)
		  {
			  translate([Sensor_3_WaterPipeHole_Location_X,Sensor_3_WaterPipeHole_Location_Y,Sensor_3_WaterPipeHole_Location_Z])
			  {
				cylinder(d=Sensor_3_WaterPipeHole_Diameter, h=Sensor_3_WaterPipeHole_Height);
			  }
		  }
			
			
			
			
		}		
		
		// add any shapes here		
	
		if(PrintSensorExtensionPipe)
		{
			PolyCyl(FloatSensorScrewHoleDiameter,4,0);
			LabelHole(X=-FloatSensorScrewHoleDiameter/2, Y=FloatSensorScrewHoleDiameter/2, Z=CapTopThickness, LabelText=FloatSensorLabelText);
		}
		
	}
}

if (Layout == "Inverted")
  translate([0,0,OAHeight])
    rotate([180,0,0])
      TubeCap();