﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace YourSmartSolutions.IoT.Devices.Sensors
{
    public class SensorBase : ISensor
    {
        public uint SensorValueReportInterval { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string DevicePartNumberIdentifier { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //public string DeviceInstanceNickname { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        //public event EventHandler SensorValueChangedReportEvent;



        #region default implementations of interfaces
        uint ISensor.SensorValueReportInterval { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        event EventHandler ISensor.SensorValueChangedReportEvent
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        string IDevice.DevicePartNumberIdentifier { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        private string m_DeviceInstanceNickname = "Generic sensor nickname not set";
        public string DeviceInstanceNickname
        {
            get { return m_DeviceInstanceNickname; }
            set { m_DeviceInstanceNickname = value; }
        }

        #endregion


        #region Automatic Sensor Value updates


        private System.Threading.Timer continuousUpdateTimer = null;

        private const int MillisecondsToWaitForFirstUpdateTimerCallback = 250; // delay a quarter of a second as default at activation

        private bool m_SendSensorDataContinuallyBasedOntimer = false;
        public bool SendSensorDataContinuallyBasedOntimer
        {
            get
            {
                return m_SendSensorDataContinuallyBasedOntimer;
            }
            set
            {
                m_SendSensorDataContinuallyBasedOntimer = value;
                if (m_SendSensorDataContinuallyBasedOntimer == true)
                {
                    startSendingSensorData(ContinuousUpdateFrequencyMilliseconds);
                }
                else
                {
                    stopSendingSensorData();
                }
            }
        }



        private void startSendingSensorData(int continuousUpdateFrequencyMilliseconds)
        {
            // make sure to stop any existing timer first
            stopSendingSensorData();

            // create and start a new timer
            continuousUpdateTimer = new Timer(continousUpdateTimer_Callback, state: null,
                dueTime: MillisecondsToWaitForFirstUpdateTimerCallback, period: continuousUpdateFrequencyMilliseconds);
        }

        private void stopSendingSensorData()
        {
            if (continuousUpdateTimer != null)
            {
                continuousUpdateTimer.Change(dueTime: Timeout.Infinite, period: Timeout.Infinite);
                continuousUpdateTimer = null;
            }
        }

        private int m_ContinuousUpdateFrequencyMilliseconds = 500;
        public int ContinuousUpdateFrequencyMilliseconds
        {
            get
            {
                return m_ContinuousUpdateFrequencyMilliseconds;
            }
            set
            {
                m_ContinuousUpdateFrequencyMilliseconds = value;

                // restart the time based on new frequency if it is running
                if (SendSensorDataContinuallyBasedOntimer)
                {
                    stopSendingSensorData();
                    startSendingSensorData(m_ContinuousUpdateFrequencyMilliseconds);
                }
            }
        }



        private void continousUpdateTimer_Callback(object state)
        {
            GetReadingFromSensorAndRaiseEventWithResults();
            //OnNewSensorReadingEvent(new NewSensorReadingEventEventArgs(DateTime.Now, MostRecentDistanceReadingInCM, AverageDistanceReadingInCM));
        }

        public virtual void GetReadingFromSensorAndRaiseEventWithResults()
        {
            string test = DeviceInstanceNickname;
            System.Diagnostics.Debug.WriteLine($"In Base, Asked to get reading for {DeviceInstanceNickname}");
        }

        #endregion



    }
}
