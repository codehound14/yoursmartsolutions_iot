﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourSmartSolutions.IoT.Devices.Sensors
{
    public interface ISensor: IDevice
    {

        uint SensorValueReportInterval { get; set; }

        event EventHandler SensorValueChangedReportEvent;


        //public abstract object GetReadingFromSensor();
    }
}
