﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.I2c;

namespace YourSmartSolutions.IoT.Devices
{
    public class I2CBus
    {

        private string m_CompositStatusString = "I2C Bus status has not been determined yet";
        public string CompositStatusString
        {
            get
            {
                m_CompositStatusString = $"Found {CountOfDevicesFound} devices and {CountOfAddressesWithNoDevice} empty addresses after {CountOfAddressesScanned} scaned addresses";
                if(DevicesThatWereFound != null && DevicesThatWereFound.Count > 0)
                {
                    foreach (byte address in DevicesThatWereFound.Keys)
                    {
                        m_CompositStatusString += $"{Environment.NewLine}{Environment.NewLine} At {address.ToString("X2")}: {DevicesThatWereFound[address]}{Environment.NewLine}";
                    }
                }
                return m_CompositStatusString;
            }
        }


        private int m_CountOfDevicesFound = -1;
        public int CountOfDevicesFound
        {
            get
            {
                return m_CountOfDevicesFound;
            }
            set
            {
                m_CountOfDevicesFound = value;
            }
        }


        private int m_CountOfAddressesWithNoDevice = -1;
        public int CountOfAddressesWithNoDevice
        {
            get
            {
                return m_CountOfAddressesWithNoDevice;
            }
            set
            {
                m_CountOfAddressesWithNoDevice = value;
            }
        }


        public int CountOfAddressesScanned
        {
            get
            {
                return m_CountOfDevicesFound + m_CountOfAddressesWithNoDevice;
            }
        }


        private Dictionary<byte, string> m_DevicesThatWereFound;
        public Dictionary<byte, string> DevicesThatWereFound
        {
            get
            {
                return m_DevicesThatWereFound;
            }
            set
            {
                m_DevicesThatWereFound = value;
            }
        }


        private Dictionary<byte, string> m_AddressesWithNoDevice;
        public Dictionary<byte, string> AddressesWithNoDevice
        {
            get
            {
                return m_AddressesWithNoDevice;
            }
            set
            {
                m_AddressesWithNoDevice = value;
            }
        }




        public async void GetReadingFromSensorAndRaiseEventWithResultsAsync()
        {
            var devicesOnI2Cbus = await ListI2CDevicesAsync();
            System.Diagnostics.Debug.WriteLine($"\n\nIn I2CBus, in GetReadingFromSensorAndRaiseEventWithResultsAsync return value is '{devicesOnI2Cbus}'");
            //List<byte> devices = devicesOnI2Cbus as List<byte>;
            //IList<byte> devicesOnI2Cbus = await I2CBus.FindDevicesAsync();
            //if (sensor == null)
            //{
            //    System.Diagnostics.Debug.WriteLine($"\n\nIn I2CBus, Asked to get reading for {DeviceInstanceNickname}, BUT WE HAVE NOT GOTTEN A CONNECTION TO THE DEVICE YET!!!");
            //}
            //else
            //{
            //    try
            //    {
            //        string test = DeviceInstanceNickname;
            //        System.Diagnostics.Debug.WriteLine($"\n\nIn I2CBus, Asked to get reading for {DeviceInstanceNickname}");

            //        // Read data from I2C.
            //        var command = new byte[1];
            //        var humidityData = new byte[2];
            //        var temperatureData = new byte[2];

            //        // Read humidity.
            //        command[0] = 0xE5;

            //        // If this next line crashes with a NullReferenceException, then
            //        // there was a sharing violation on the device. (See StartScenarioAsync above.)
            //        //
            //        // If this next line crashes for some other reason, then there was
            //        // an error accessing the device.
            //        sensor.WriteRead(command, humidityData);

            //        // Read temperature.
            //        command[0] = 0xE3;
            //        // If this next line crashes, then there was an error accessing the sensor.
            //        sensor.WriteRead(command, temperatureData);

            //        // Calculate and report the humidity.
            //        var rawHumidityReading = humidityData[0] << 8 | humidityData[1];
            //        var humidityRatio = rawHumidityReading / (float)65536;
            //        double humidity = -6 + (125 * humidityRatio);
            //        MostRecentHumidityReading = humidity;
            //        //CurrentHumidity.Text = humidity.ToString();

            //        // Calculate and report the temperature.
            //        var rawTempReading = temperatureData[0] << 8 | temperatureData[1];
            //        var tempRatio = rawTempReading / (float)65536;
            //        double temperature = (-46.85 + (175.72 * tempRatio)) * 9 / 5 + 32;
            //        MostRecentTemperatureReading = temperature;
            //        //CurrentTemp.Text = temperature.ToString();

            //        OnNewReadingEvent(new NewReadingEventEventArgs(DateTime.Now, NumberOfI2CDevices));
            //    catch (Exception exception)
            //    {
            //        System.Diagnostics.Debug.WriteLine($"In I2CBus, Exception in GetReadingFromSensorAndRaiseEventWithResults due to: {exception.Message}");
            //    }

            //}
        }


        public async Task<IEnumerable<byte>> ListI2CDevicesAsync()
        {

            byte multiplexerAddress = 0x70;
            try
            {
                await Tca9548a.Initialize(multiplexerAddress);
                Tca9548a.SelectAddress(multiplexerAddress, 0x1);
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine($"In I2CBus, Failed to set TCA9548A to address {multiplexerAddress.ToString("X2")} Exception in ListI2CDevicesAsync due to: {exception.Message}");

            }

            IList<byte> returnValue = new List<byte>();

            Dictionary<byte, string> CurrentDevicesThatWereFound = new Dictionary<byte, string>();
            Dictionary<byte, string> CurrentAddressesWithNoDevice = new Dictionary<byte, string>();

            // *** 
            // *** Get a selector string that will return all I2C controllers on the system
            // *** 
            string aqs = I2cDevice.GetDeviceSelector();

            // *** 
            // *** Find the I2C bus controller device with our selector string 
            // *** 
            DeviceInformationCollection deviceInformationCollection = await DeviceInformation.FindAllAsync(aqs).AsTask();

            if (deviceInformationCollection.Count > 0)
            {
                //const int minimumAddress = 0x16;
                //const int maximumAddress = 0x42;
                const int minimumAddress = 0x08;
                const int maximumAddress = 0x77;

                DeviceInformation hostDeviceInformation = deviceInformationCollection[0];
                System.Diagnostics.Debug.WriteLine($"In I2CBus.ListI2CDevicesAsync, Name = '{hostDeviceInformation.Name}'");
                System.Diagnostics.Debug.WriteLine($"In I2CBus.ListI2CDevicesAsync, Id = '{hostDeviceInformation.Id}'");
                System.Diagnostics.Debug.WriteLine($"In I2CBus.ListI2CDevicesAsync, PropertiesCount = '{hostDeviceInformation.Properties.Count}'");

                for (byte address = minimumAddress; address <= maximumAddress; address++)
                {
                    var i2cConnectionSettings = new I2cConnectionSettings(address);
                    //settings.BusSpeed = I2cBusSpeed.FastMode;
                    i2cConnectionSettings.SharingMode = I2cSharingMode.Shared;

                    // *** 
                    // *** Create an I2cDevice with our selected bus controller and I2C settings 
                    // *** 
                    using (I2cDevice device = await I2cDevice.FromIdAsync(deviceInformationCollection[0].Id, i2cConnectionSettings))
                    {
                        if (device != null)
                        {
                            try
                            {
                                if (address == 0x40)
                                {
                                    // Read data from I2C as if it was an HTU21D.
                                    var command = new byte[1];
                                    var humidityData = new byte[2];
                                    var temperatureData = new byte[2];

                                    // Read humidity.
                                    command[0] = 0xE5;

                                    // If this next line crashes with a NullReferenceException, then
                                    // there was a sharing violation on the device. (See StartScenarioAsync above.)
                                    //
                                    // If this next line crashes for some other reason, then there was
                                    // an error accessing the device.
                                    device.WriteRead(command, humidityData);

                                    // Read temperature.
                                    command[0] = 0xE3;
                                    // If this next line crashes, then there was an error accessing the sensor.
                                    device.WriteRead(command, temperatureData);

                                    CurrentDevicesThatWereFound.Add(address, "Found what looks like an HTU21D sensor");
                                }
                                else
                                {
                                    // write 0 as a generic test
                                    byte[] writeBuffer = new byte[1] { 0 };
                                    device.Write(writeBuffer);
                                    CurrentDevicesThatWereFound.Add(address, "Found but type not known");
                                }

                                //byte[] readBuffer = new byte[15];
                                //device.Read(readBuffer);

                                //// example of a test for a specific ID or other special identifying data to refine the search
                                //if (readBuffer[0] == 0x2D)
                                //{
                                //    returnValue.Add(address);
                                //}

                                // *** 
                                // *** If no exception is thrown, there is 
                                // *** a device at this address. 
                                // *** 
                                returnValue.Add(address);
                            }
                            catch (System.IO.FileNotFoundException fileNotFoundException)
                            {
                                // *** 
                                // *** If the address is invalid, an exception will be thrown. 
                                // *** 
                                string exceptionMessage = fileNotFoundException.Message;
                                //System.Diagnostics.Debug.WriteLine($"In I2CBus,  For address {address.ToString("X2")} Exception in ListI2CDevicesAsync due to: {fileNotFoundException.Message}");
                                CurrentAddressesWithNoDevice.Add(address, "No device found at this address");
                            }
                            catch (Exception exception)
                            {
                                // *** 
                                // *** If the address is invalid, an exception will be thrown. 
                                // *** 
                                System.Diagnostics.Debug.WriteLine($"In I2CBus, For address {address.ToString("X2")} Exception in ListI2CDevicesAsync due to: {exception.Message}");
                                CurrentDevicesThatWereFound.Add(address, $"Strange device found at this address.  Unforseen exeption '{exception.Message}'");
                            }
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine($"In I2CBus, For address {address.ToString("X2")} Problem in ListI2CDevicesAsync due to NULL device");
                        }
                    }
                }
            }

            AddressesWithNoDevice = CurrentAddressesWithNoDevice;
            CountOfAddressesWithNoDevice = AddressesWithNoDevice.Count;

            DevicesThatWereFound = CurrentDevicesThatWereFound;
            CountOfDevicesFound = DevicesThatWereFound.Count;

            OnNewReadingEvent(new NewReadingEventEventArgs(DateTime.Now, CountOfDevicesFound));

            return returnValue;
        }


        #region NewReadingEvent Event and Delegate
        public class NewReadingEventEventArgs : System.ComponentModel.CancelEventArgs
        {
            public NewReadingEventEventArgs(DateTime TimeOfReading, int NumberOfI2CDevices)
            {
                this.m_TimeOfReading = TimeOfReading;
                this.m_NumberOfI2CDevices = NumberOfI2CDevices;
            }


            private int m_NumberOfI2CDevices;
            public int NumberOfI2CDevices
            {
                get
                {
                    return m_NumberOfI2CDevices;
                }
                set
                {
                    m_NumberOfI2CDevices = value;
                }
            }

            private DateTime m_TimeOfReading;
            public DateTime TimeOfReading
            {
                get
                {
                    return m_TimeOfReading;
                }
                set
                {
                    m_TimeOfReading = value;
                }
            }
        }


        public delegate void NewReadingEventHandler(object sender,
          NewReadingEventEventArgs EventArgsParam);


        public event NewReadingEventHandler NewReadingEvent;


        protected virtual void OnNewReadingEvent(NewReadingEventEventArgs EventArgsParam)
        {
            if (NewReadingEvent != null)
            {
                // invoke the delegate
                NewReadingEvent(this, EventArgsParam);
                if (EventArgsParam.Cancel == true)
                {
                    throw new Exception("This is not a cancellable event!");
                }
            }
        }
        #endregion



        /// <summary>
        /// 
        /// </summary>
        /// <see cref="https://www.hackster.io/porrey/discover-i2c-devices-on-the-raspberry-pi-84bc8b"/>
        public static async Task<IEnumerable<byte>> FindDevicesAsync()
        {
            IList<byte> returnValue = new List<byte>();

            // *** 
            // *** Get a selector string that will return all I2C controllers on the system
            // *** 
            string aqs = I2cDevice.GetDeviceSelector();

            // *** 
            // *** Find the I2C bus controller device with our selector string 
            // *** 
            var dis = await DeviceInformation.FindAllAsync(aqs).AsTask();

            if (dis.Count > 0)
            {
                const int minimumAddress = 0x08;
                const int maximumAddress = 0x77;

                for (byte address = minimumAddress; address <= maximumAddress; address++)
                {
                    var settings = new I2cConnectionSettings(address);
                    settings.BusSpeed = I2cBusSpeed.FastMode;
                    settings.SharingMode = I2cSharingMode.Shared;

                    // *** 
                    // *** Create an I2cDevice with our selected bus controller and I2C settings 
                    // *** 
                    using (I2cDevice device = await I2cDevice.FromIdAsync(dis[0].Id, settings))
                    {
                        if (device != null)
                        {
                            try
                            {
                                byte[] writeBuffer = new byte[1] { 0 };
                                device.Write(writeBuffer);

                                // *** 
                                // *** If no exception is thrown, there is 
                                // *** a device at this address. 
                                // *** 
                                returnValue.Add(address);
                            }
                            catch(Exception)
                            {
                                // *** 
                                // *** If the address is invalid, an exception will be thrown. 
                                // *** 
                            }
                        }
                    }
                }
            }

            return returnValue;
        }

    }
}
