﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourSmartSolutions.IoT.Devices
{
    public interface IDevice
    {
        string DevicePartNumberIdentifier { get; set; }
        string DeviceInstanceNickname { get; set; }
    }
}
