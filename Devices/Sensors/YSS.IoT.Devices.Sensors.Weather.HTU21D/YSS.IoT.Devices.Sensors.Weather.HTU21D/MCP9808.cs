﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.I2c;

namespace YSS.IoT.Devices.Sensors.Weather
{
    /// <summary>
    /// 
    /// </summary>
    /// <see cref="https://learn.adafruit.com/adafruit-mcp9808-precision-i2c-temperature-sensor-guide/pinouts"/>
    /// <seealso Very cool but stale library in .net cref="https://github.com/porrey/IoT/tree/master/source/IoT%20Devices%20and%20Sensors/Porrey.Uwp.IoT.Sensors.Mcp9808"/>
    /// <example>
    /// A0 A1 A2 - These are the address select pins. Since you can only have one device with a given address on an i2c bus, there must be a way to adjust the address if you want to put more than one MCP9808 on a shared i2c bus. The A0/A1/A2 pins set the bottom three pins of the i2c address. There are pull-down resistors on the board so connect them to VDD to set the bits to '1'. They are read on power up, so de-power and re-power to reset the address
    /// The default address is 0x18 and the address can be calculated by 'adding' the A0/A1/A2 to the base of 0x18 A0 sets the lowest bit with a value of 1, A1 sets the middle bit with a value of 2 and A2 sets the high bit with a value of 4. The final address is 0x18 + A2 + A1 + A0.
    ///     So for example if A2 is tied to VDD and A0 is tied to VDD, the address is 0x18 + 4 + 1 = 0x1D. 
    ///     If only A0 is tied to VDD, the address is 0x18 + 1 = 0x19
    ///     If only A1 is tied to VDD, the address is 0x18 + 2 = 0x1A
    ///     If only A2 is tied to VDD, the address is 0x18 + 4 = 0x1C
    /// </example>
    public class MCP9808 : YourSmartSolutions.IoT.Devices.Sensors.SensorBase
    {
        public MCP9808()
        {
            ContinuousUpdateFrequencyMilliseconds = 3000;
            GetI2CConnectionToSensor();

            //IoT.Devices.Sensors.Weather.MCP9808 mCP9808 = new MCP9808();
        }


        // See Comments on class for Device Address 
        private int m_DeviceI2CAddress = 0x18;
        public int DeviceI2CAddress
        {
            get
            {
                return m_DeviceI2CAddress;
            }
            set
            {
                m_DeviceI2CAddress = value;
            }
        }


        private double m_MostRecentTemperatureReading;
        public double MostRecentTemperatureReading
        {
            get
            {
                return m_MostRecentTemperatureReading;
            }
            set
            {
                m_MostRecentTemperatureReading = value;
                //System.Diagnostics.Debug.WriteLine($"In MCP9808, Most recent Temperature Reading {m_MostRecentTemperatureReading}");
            }
        }


        private double m_MostRecentHumidityReading;
        public double MostRecentHumidityReading
        {
            get
            {
                return m_MostRecentHumidityReading;
            }
            set
            {
                m_MostRecentHumidityReading = value;
                //System.Diagnostics.Debug.WriteLine($"In MCP9808, Most recent Humidity Reading {m_MostRecentHumidityReading} (Not Valid for MCP9808)");
            }
        }



        private I2cDevice sensor = null;

        private async Task GetI2CConnectionToSensor()
        {
            // release old connection if we already had one
            if (sensor != null)
            {
                ReleaseI2CConnectionToSensor();
            }

            try
            {

                string i2cDeviceSelector = I2cDevice.GetDeviceSelector();
                IReadOnlyList<DeviceInformation> devices = await DeviceInformation.FindAllAsync(i2cDeviceSelector);

                int devicesCount = devices.Count;
                //System.Diagnostics.Debug.WriteLine($"In MCP9808.GetI2CConnectionToSensor, deviceCount = '{devicesCount}'");

                var i2cConnectionSettings = new I2cConnectionSettings(DeviceI2CAddress);
                //settings.BusSpeed = I2cBusSpeed.FastMode;
                i2cConnectionSettings.SharingMode = I2cSharingMode.Shared;

                // If this next line crashes with an ArgumentOutOfRangeException,
                // then the problem is that no I2C devices were found.
                //
                // If the next line crashes with Access Denied, then the problem is
                // that access to the I2C device (our target sensor) is denied.
                //
                // The call to FromIdAsync will also crash if the settings are invalid.
                //
                // FromIdAsync produces null if there is a sharing violation on the device.
                // This will result in a NullReferenceException in Timer_Tick below.
                sensor = await I2cDevice.FromIdAsync(devices[0].Id, i2cConnectionSettings);

                //DeviceInformation deviceInformation = devices[0];
                //System.Diagnostics.Debug.WriteLine($"In MCP9808.GetI2CConnectionToSensor, Name = '{deviceInformation.Name}'");
                //System.Diagnostics.Debug.WriteLine($"In MCP9808.GetI2CConnectionToSensor, Id = '{deviceInformation.Id}'");
                //System.Diagnostics.Debug.WriteLine($"In MCP9808.GetI2CConnectionToSensor, PropertiesCount = '{deviceInformation.Properties.Count}'");
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine($"In MCP9808, Exception while connecting to sensor due to: {exception.Message}");
            }

        }

        private void ReleaseI2CConnectionToSensor()
        {
            // Release the I2C sensor.
            if (sensor != null)
            {
                sensor.Dispose();
                sensor = null;
            }
        }

        // set the device specific ID
        new public string DevicePartNumberIdentifier { get { return "MCP9808"; } }


        public override void GetReadingFromSensorAndRaiseEventWithResults()
        {
            if (sensor == null)
            {
                System.Diagnostics.Debug.WriteLine($"\n\nIn MCP9808, Asked to get reading for {DeviceInstanceNickname}, BUT WE HAVE NOT GOTTEN A CONNECTION TO THE DEVICE YET!!!");
            }
            else
            {
                try
                {
                    string test = DeviceInstanceNickname;
                    //System.Diagnostics.Debug.WriteLine($"\n\nIn MCP9808, Asked to get reading for {DeviceInstanceNickname}");

                    // Read data from I2C.
                    var command = new byte[1];
                    var humidityData = new byte[2];
                    var temperatureData = new byte[2];

                    //////// Read humidity.
                    //////command[0] = 0xE5;

                    //////// If this next line crashes with a NullReferenceException, then
                    //////// there was a sharing violation on the device. (See StartScenarioAsync above.)
                    ////////
                    //////// If this next line crashes for some other reason, then there was
                    //////// an error accessing the device.
                    //////sensor.WriteRead(command, humidityData); ljsdflsjdflsjdflksjdflj

                    // Read temperature.
                    command[0] = 0xE3;
                    // If this next line crashes, then there was an error accessing the sensor.
                    sensor.WriteRead(command, temperatureData);

                    // Calculate and report the humidity.
                    var rawHumidityReading = humidityData[0] << 8 | humidityData[1];
                    var humidityRatio = rawHumidityReading / (float)65536;
                    double humidity = -6 + (125 * humidityRatio);
                    MostRecentHumidityReading = humidity;
                    //CurrentHumidity.Text = humidity.ToString();

                    // Calculate and report the temperature.
                    var rawTempReading = temperatureData[0] << 8 | temperatureData[1];
                    var tempRatio = rawTempReading / (float)65536;
                    double temperature = (-46.85 + (175.72 * tempRatio)) * 9 / 5 + 32;
                    MostRecentTemperatureReading = temperature;
                    //CurrentTemp.Text = temperature.ToString();

                    OnNewSensorReadingEvent(new NewSensorReadingEventEventArgs(DateTime.Now, MostRecentTemperatureReading, MostRecentHumidityReading));
                }
                catch (Exception exception)
                {
                    System.Diagnostics.Debug.WriteLine($"{Environment.NewLine}{Environment.NewLine}In MCP9808, Exception in GetReadingFromSensorAndRaiseEventWithResults due to: {exception.Message}");
                }

            }
        }





        #region NewSensorReadingEvent Event and Delegate
        public class NewSensorReadingEventEventArgs : System.ComponentModel.CancelEventArgs
        {
            public NewSensorReadingEventEventArgs(DateTime TimeOfSensorReading, double TemperatureReadingInCelcius, double HumidityReading)
            {
                this.m_TimeOfSensorReading = TimeOfSensorReading;
                this.m_TemperatureReadingInCelcius = TemperatureReadingInCelcius;
                this.m_HumidityReading = HumidityReading;
            }


            private double m_TemperatureReadingInCelcius;
            public double TemperatureReadingInCelcius
            {
                get
                {
                    return m_TemperatureReadingInCelcius;
                }
                set
                {
                    m_TemperatureReadingInCelcius = value;
                }
            }


            private double m_HumidityReading;
            public double HumidityReading
            {
                get
                {
                    return m_HumidityReading;
                }
                set
                {
                    m_HumidityReading = value;
                }
            }



            private DateTime m_TimeOfSensorReading;
            public DateTime TimeOfSensorReading
            {
                get
                {
                    return m_TimeOfSensorReading;
                }
                set
                {
                    m_TimeOfSensorReading = value;
                }
            }
        }


        public delegate void NewSensorReadingEventHandler(object sender,
          NewSensorReadingEventEventArgs EventArgsParam);


        public event NewSensorReadingEventHandler NewSensorReadingEvent;


        protected virtual void OnNewSensorReadingEvent(NewSensorReadingEventEventArgs EventArgsParam)
        {
            if (NewSensorReadingEvent != null)
            {
                // invoke the delegate
                NewSensorReadingEvent(this, EventArgsParam);
                if (EventArgsParam.Cancel == true)
                {
                    throw new Exception("This is not a cancellable event!");
                }
            }
        }
        #endregion
    }
}
