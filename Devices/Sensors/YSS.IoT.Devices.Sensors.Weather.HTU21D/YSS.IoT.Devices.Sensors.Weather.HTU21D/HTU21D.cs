﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.I2c;

namespace YSS.IoT.Devices.Sensors.Weather.HTU21D
{
    /// <summary>
    /// The single fixed address of 0x40 was determined by looking at the datasheet for the HTU21D sensor.
    /// </summary>
    public class HTU21D : YourSmartSolutions.IoT.Devices.Sensors.SensorBase
    {
        public HTU21D()
        {
            ContinuousUpdateFrequencyMilliseconds = 3000;
            GetI2CConnectionToSensor();
        }


        // See Comments on class for Device Address 
        private int m_DeviceI2CAddress = 0x40;
        public int DeviceI2CAddress
        {
            get
            {
                return m_DeviceI2CAddress;
            }
            set
            {
                m_DeviceI2CAddress = value;
            }
        }


        private double m_MostRecentTemperatureReading;
        public double MostRecentTemperatureReading
        {
            get
            {
                return m_MostRecentTemperatureReading;
            }
            set
            {
                m_MostRecentTemperatureReading = value;
                //System.Diagnostics.Debug.WriteLine($"In HTU21D, Most recent Temperature Reading {m_MostRecentTemperatureReading}");
            }
        }


        private double m_MostRecentHumidityReading;
        public double MostRecentHumidityReading
        {
            get
            {
                return m_MostRecentHumidityReading;
            }
            set
            {
                m_MostRecentHumidityReading = value;
                //System.Diagnostics.Debug.WriteLine($"In HTU21D, Most recent Humidity Reading {m_MostRecentHumidityReading}");
            }
        }



        private I2cDevice sensor = null;

        private async Task GetI2CConnectionToSensor()
        {
            // release old connection if we already had one
            if (sensor != null)
            {
                ReleaseI2CConnectionToSensor();
            }

            try
            {

                string i2cDeviceSelector = I2cDevice.GetDeviceSelector();
                IReadOnlyList<DeviceInformation> devices = await DeviceInformation.FindAllAsync(i2cDeviceSelector);

                int devicesCount = devices.Count;
                //System.Diagnostics.Debug.WriteLine($"In HTU21D.GetI2CConnectionToSensor, deviceCount = '{devicesCount}'");

                var i2cConnectionSettings = new I2cConnectionSettings(DeviceI2CAddress);
                //settings.BusSpeed = I2cBusSpeed.FastMode;
                i2cConnectionSettings.SharingMode = I2cSharingMode.Shared;

                // If this next line crashes with an ArgumentOutOfRangeException,
                // then the problem is that no I2C devices were found.
                //
                // If the next line crashes with Access Denied, then the problem is
                // that access to the I2C device (our target sensor) is denied.
                //
                // The call to FromIdAsync will also crash if the settings are invalid.
                //
                // FromIdAsync produces null if there is a sharing violation on the device.
                // This will result in a NullReferenceException in Timer_Tick below.
                sensor = await I2cDevice.FromIdAsync(devices[0].Id, i2cConnectionSettings);

                //DeviceInformation deviceInformation = devices[0];
                //System.Diagnostics.Debug.WriteLine($"In HTU21D.GetI2CConnectionToSensor, Name = '{deviceInformation.Name}'");
                //System.Diagnostics.Debug.WriteLine($"In HTU21D.GetI2CConnectionToSensor, Id = '{deviceInformation.Id}'");
                //System.Diagnostics.Debug.WriteLine($"In HTU21D.GetI2CConnectionToSensor, PropertiesCount = '{deviceInformation.Properties.Count}'");
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine($"In HTU21D, Exception while connecting to sensor due to: {exception.Message}");
            }

        }

        private void ReleaseI2CConnectionToSensor()
        {
            // Release the I2C sensor.
            if (sensor != null)
            {
                sensor.Dispose();
                sensor = null;
            }
        }

        // set the device specific ID
        new public string DevicePartNumberIdentifier { get { return "HTU21D"; } }


        public override void GetReadingFromSensorAndRaiseEventWithResults()
        {
            if (sensor == null)
            {
                System.Diagnostics.Debug.WriteLine($"\n\nIn HTU21D, Asked to get reading for {DeviceInstanceNickname}, BUT WE HAVE NOT GOTTEN A CONNECTION TO THE DEVICE YET!!!");
            }
            else
            {
                try
                {
                    //System.Diagnostics.Debug.WriteLine($"\n\nIn HTU21D, Asked to get reading for {DeviceInstanceNickname}");

                    // Read data from I2C.
                    var command = new byte[1];
                    var humidityData = new byte[2];
                    var temperatureData = new byte[2];

                    // Read humidity.
                    command[0] = 0xE5;

                    // If this next line crashes with a NullReferenceException, then
                    // there was a sharing violation on the device. (See StartScenarioAsync above.)
                    //
                    // If this next line crashes for some other reason, then there was
                    // an error accessing the device.
                    sensor.WriteRead(command, humidityData);

                    // Read temperature.
                    command[0] = 0xE3;
                    // If this next line crashes, then there was an error accessing the sensor.
                    sensor.WriteRead(command, temperatureData);

                    // Calculate and report the humidity.
                    var rawHumidityReading = humidityData[0] << 8 | humidityData[1];
                    var humidityRatio = rawHumidityReading / (float)65536;
                    double humidity = -6 + (125 * humidityRatio);
                    MostRecentHumidityReading = humidity;
                    //CurrentHumidity.Text = humidity.ToString();

                    // Calculate and report the temperature.
                    var rawTempReading = temperatureData[0] << 8 | temperatureData[1];
                    var tempRatio = rawTempReading / (float)65536;
                    double temperature = (-46.85 + (175.72 * tempRatio)) * 9 / 5 + 32;
                    MostRecentTemperatureReading = temperature;
                    //CurrentTemp.Text = temperature.ToString();

                    OnNewSensorReadingEvent(new NewSensorReadingEventEventArgs(DateTime.Now, MostRecentTemperatureReading, MostRecentHumidityReading));
                }
                catch (Exception exception)
                {
                    System.Diagnostics.Debug.WriteLine($"In HTU21D, Exception in GetReadingFromSensorAndRaiseEventWithResults due to: {exception.Message}");
                }

            }
        }





        #region NewSensorReadingEvent Event and Delegate
        public class NewSensorReadingEventEventArgs : System.ComponentModel.CancelEventArgs
        {
            public NewSensorReadingEventEventArgs(DateTime TimeOfSensorReading, double TemperatureReadingInCelcius, double HumidityReading)
            {
                this.m_TimeOfSensorReading = TimeOfSensorReading;
                this.m_TemperatureReadingInCelcius = TemperatureReadingInCelcius;
                this.m_HumidityReading = HumidityReading;
            }


            private double m_TemperatureReadingInCelcius;
            public double TemperatureReadingInCelcius
            {
                get
                {
                    return m_TemperatureReadingInCelcius;
                }
                set
                {
                    m_TemperatureReadingInCelcius = value;
                }
            }


            private double m_HumidityReading;
            public double HumidityReading
            {
                get
                {
                    return m_HumidityReading;
                }
                set
                {
                    m_HumidityReading = value;
                }
            }



            private DateTime m_TimeOfSensorReading;
            public DateTime TimeOfSensorReading
            {
                get
                {
                    return m_TimeOfSensorReading;
                }
                set
                {
                    m_TimeOfSensorReading = value;
                }
            }
        }


        public delegate void NewSensorReadingEventHandler(object sender,
          NewSensorReadingEventEventArgs EventArgsParam);


        public event NewSensorReadingEventHandler NewSensorReadingEvent;


        protected virtual void OnNewSensorReadingEvent(NewSensorReadingEventEventArgs EventArgsParam)
        {
            if (NewSensorReadingEvent != null)
            {
                // invoke the delegate
                NewSensorReadingEvent(this, EventArgsParam);
                if (EventArgsParam.Cancel == true)
                {
                    throw new Exception("This is not a cancellable event!");
                }
            }
        }
        #endregion


        #region sample code from David Porrey
        public static async Task<IEnumerable<byte>> FindDevicesAsync()
        {
            IList<byte> returnValue = new List<byte>();

            // *** 
            // *** Get a selector string that will return all I2C controllers on the system
            // *** 
            string aqs = I2cDevice.GetDeviceSelector();

            // *** 
            // *** Find the I2C bus controller device with our selector string 
            // *** 
            var dis = await DeviceInformation.FindAllAsync(aqs).AsTask();

            if (dis.Count > 0)
            {
                const int minimumAddress = 0x08;
                const int maximumAddress = 0x77;

                for (byte address = minimumAddress; address <= maximumAddress; address++)
                {
                    var settings = new I2cConnectionSettings(address);
                    settings.BusSpeed = I2cBusSpeed.FastMode;
                    settings.SharingMode = I2cSharingMode.Shared;

                    // *** 
                    // *** Create an I2cDevice with our selected bus controller and I2C settings 
                    // *** 
                    using (I2cDevice device = await I2cDevice.FromIdAsync(dis[0].Id, settings))
                    {
                        if (device != null)
                        {
                            try
                            {
                                byte[] writeBuffer = new byte[1] { 0 };
                                device.Write(writeBuffer);

                                // *** 
                                // *** If no exception is thrown, there is 
                                // *** a device at this address. 
                                // *** 
                                returnValue.Add(address);
                            }
                            catch
                            {
                                // *** 
                                // *** If the address is invalid, an exception will be thrown. 
                                // *** 
                            }
                        }
                    }
                }
            }

            return returnValue;
        }
        #endregion
    }
}
