﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourSmartSolutions.IoT.Devices.Sensors.UltrasonicRangeSensors
{
    public class UltrasonicRangeSensorPinConnections
    {
        public int? Ground { get; set; }
        public int? VCC { get; set; }
        public int? Trigger { get; set; }
        public int? Echo { get; set; }
    }
}
