﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Gpio;

/// <see cref="https://stackoverflow.com/questions/30124861/ultrasonic-sensor-raspberry-pi-2-c-sharp-net"/>
/// <seealso cref="="https://jeremylindsayni.wordpress.com/2016/06/01/using-the-hc-sr04-range-finder-with-c-and-the-raspberry-pi/"/>
/// <seealso cref="https://uwpstories.wordpress.com/2015/12/02/hc-sr04-ultrasonic-sensor-on-raspberry-pi-2/"/>

namespace YourSmartSolutions.IoT.Devices.Sensors.UltrasonicRangeSensors
{
    public class UltrasonicRangeSensor : SensorBase
    {

        #region NewSensorReadingEvent Event and Delegate
        public class NewSensorReadingEventEventArgs : System.ComponentModel.CancelEventArgs
        {
            public NewSensorReadingEventEventArgs(DateTime TimeOfSensorReading, double DistanceReadingInCM, double DistanceReadingAverageInCM)
            {
                this.m_TimeOfSensorReading = TimeOfSensorReading;
                this.m_DistanceReadingInCM = DistanceReadingInCM;
                this.m_DistanceReadingAverageInCM = DistanceReadingAverageInCM;
            }


            private double m_DistanceReadingInCM;
            public double DistanceReadingInCM
            {
                get
                {
                    return m_DistanceReadingInCM;
                }
                set
                {
                    m_DistanceReadingInCM = value;
                }
            }


            private double m_DistanceReadingAverageInCM;
            public double DistanceReadingAverageInCM
            {
                get
                {
                    return m_DistanceReadingAverageInCM;
                }
                set
                {
                    m_DistanceReadingAverageInCM = value;
                }
            }



            private DateTime m_TimeOfSensorReading;
            public DateTime TimeOfSensorReading
            {
                get
                {
                    return m_TimeOfSensorReading;
                }
                set
                {
                    m_TimeOfSensorReading = value;
                }
            }
        }


        public delegate void NewSensorReadingEventHandler(object sender,
          NewSensorReadingEventEventArgs EventArgsParam);


        public event NewSensorReadingEventHandler NewSensorReadingEvent;


        protected virtual void OnNewSensorReadingEvent(NewSensorReadingEventEventArgs EventArgsParam)
        {
            if (NewSensorReadingEvent != null)
            {
                // invoke the delegate
                NewSensorReadingEvent(this, EventArgsParam);
                if (EventArgsParam.Cancel == true)
                {
                    throw new Exception("This is not a cancellable event!");
                }
            }
        }
        #endregion








        private bool m_SendSensorDataContinuallyBasedOntimer = false;
        public bool SendSensorDataContinuallyBasedOntimer
        {
            get
            {
                return m_SendSensorDataContinuallyBasedOntimer;
            }
            set
            {
                m_SendSensorDataContinuallyBasedOntimer = value;
                if(m_SendSensorDataContinuallyBasedOntimer == true)
                {
                    startSendingSensorData(ContinuousUpdateFrequencyMilliseconds);
                }
                else
                {
                    stopSendingSensorData();
                }
            }
        }

        private void startSendingSensorData(int continuousUpdateFrequencyMilliseconds)
        {
            // make sure to stop any existing timer first
            stopSendingSensorData();

            // create and start a new timer
            continuousUpdateTimer = new Timer(continousUpdateTimer_Callback, state: null,
                dueTime: MillisecondsToWaitForFirstUpdateTimerCallback, period: continuousUpdateFrequencyMilliseconds);
        }

        private void stopSendingSensorData()
        {
            if(continuousUpdateTimer != null)
            {
                continuousUpdateTimer.Change(dueTime: Timeout.Infinite, period: Timeout.Infinite);
                continuousUpdateTimer = null;
            }
        }

        private int m_ContinuousUpdateFrequencyMilliseconds = 500;
        public int ContinuousUpdateFrequencyMilliseconds
        {
            get
            {
                return m_ContinuousUpdateFrequencyMilliseconds;
            }
            set
            {
                m_ContinuousUpdateFrequencyMilliseconds = value;

                // restart the time based on new frequency if it is running
                if(SendSensorDataContinuallyBasedOntimer)
                {
                    stopSendingSensorData();
                    startSendingSensorData(m_ContinuousUpdateFrequencyMilliseconds);
                }
            }
        }

        private static int DEFAULT_NumberOfItemsAverageForSmoothingReadings = 10;
        private int m_NumberOfItemsAverageForSmoothingReadings = DEFAULT_NumberOfItemsAverageForSmoothingReadings;
        public int NumberOfItemsAverageForSmoothingReadings
        {
            get
            {
                return m_NumberOfItemsAverageForSmoothingReadings;
            }
            set
            {
                m_NumberOfItemsAverageForSmoothingReadings = value;
            }
        }



        private const double ULTRASONIC_RANGE_MICROSECONDS_DURATION_OF_PULSE_TO_SEND = 10.0;
        private const double ULTRASONIC_RANGE_MILLISECONDS_DURATION_OF_PULSE_TO_SEND = ULTRASONIC_RANGE_MICROSECONDS_DURATION_OF_PULSE_TO_SEND / 1000;
        private const double ULTRASONIC_RANGE_FINDER_MIN_RANGE_CM = 2.0;
        private const double ULTRASONIC_RANGE_FINDER_MAX_RANGE_CM = 400.0;
        private const double MILLISECONDS_PER_SECOND = 1000.0;
        private const double CENTIMETERS_PER_SECOND = 34300.0;

        private const int MillisecondsToWaitForFirstUpdateTimerCallback = 250; // delay a quarter of a second as default at activation

        private GpioPin TriggerPin { get; set; }
        private GpioPin EchoPin { get; set; }

        private Stopwatch stopwatch;

        private System.Threading.Timer continuousUpdateTimer = null;

        private double MillisecondsToCentimeters(double milliseconds)
        {
            return milliseconds * MILLISECONDS_PER_SECOND * CENTIMETERS_PER_SECOND;
        }

        private double CentimetersToMilliseconds(double centimeters)
        {
            return (centimeters / CENTIMETERS_PER_SECOND) * MILLISECONDS_PER_SECOND;
        }

        private double MaxMillisecondsForRoundTrip()
        {
            return CentimetersToMilliseconds(ULTRASONIC_RANGE_FINDER_MAX_RANGE_CM);
        }

        public UltrasonicRangeSensor(UltrasonicRangeSensorPinConnections ultrasonicRangeSensorPinConnections) : this(GpioController.GetDefault(), ultrasonicRangeSensorPinConnections.Trigger, ultrasonicRangeSensorPinConnections.Echo)
        {
        }

        public UltrasonicRangeSensor(int? triggerPin, int? echoPin) : this(GpioController.GetDefault(), triggerPin, echoPin)
        {
        }

        public UltrasonicRangeSensor(GpioController gpioController, int? triggerPin, int? echoPin)
        {
            // Show an error if there is no GPIO controller
            if (gpioController == null)
            {
                // need to set status here
                //GpioStatusTop.Text = "There is no GPIO controller on this device.";
                return;
            }
            //GpioController gpioController = GpioController.GetDefault();
            stopwatch = new Stopwatch();

            //initialize trigger pin.
            this.TriggerPin = gpioController.OpenPin(triggerPin ?? -1);
            this.TriggerPin.SetDriveMode(GpioPinDriveMode.Output);
            this.TriggerPin.Write(GpioPinValue.Low);

            //initialize echo pin.
            this.EchoPin = gpioController.OpenPin(echoPin ?? -1);
            this.EchoPin.SetDriveMode(GpioPinDriveMode.Input);




            /// TODO: Create option to turn on timer to desired update interval
            /// <see cref="https://uwpstories.wordpress.com/2015/12/02/hc-sr04-ultrasonic-sensor-on-raspberry-pi-2/"/>
            //            timer = new DispatcherTimer();
            //            timer.Interval = TimeSpan.FromMilliseconds(400);
            //            timer.Tick += Timer_Tick;
            //            if (pinEcho != null & amp; amp; amp; amp; amp; &amp; amp; amp; amp; amp; pinTrigger != null)
            //{
            //                timer.Start();
            //            }

            // create timer but do not start it yet
            continuousUpdateTimer = new Timer(continousUpdateTimer_Callback, state: null,
                dueTime: Timeout.Infinite, period: Timeout.Infinite);

        }


        MathNet.Numerics.Statistics.MovingStatistics movingStatisticsOfDistanceReadings =
            new MathNet.Numerics.Statistics.MovingStatistics(DEFAULT_NumberOfItemsAverageForSmoothingReadings);


        private double m_MostRecentDistanceReadingInCM;
        public double MostRecentDistanceReadingInCM
        {
            get
            {
                return m_MostRecentDistanceReadingInCM;
            }
            private set
            {
                m_MostRecentDistanceReadingInCM = value;
                movingStatisticsOfDistanceReadings.Push(m_MostRecentDistanceReadingInCM);
                m_AverageDistanceReadingInCM = movingStatisticsOfDistanceReadings.Mean;
            }
        }


        private double m_AverageDistanceReadingInCM;
        public double AverageDistanceReadingInCM
        {
            get
            {
                return m_AverageDistanceReadingInCM;
            }
            private set
            {
                m_AverageDistanceReadingInCM = value;
            }
        }



        private void continousUpdateTimer_Callback(object state)
        {
            double DistanceReading = GetDistanceOfObjectFromSensor();
            OnNewSensorReadingEvent(new NewSensorReadingEventEventArgs(DateTime.Now, MostRecentDistanceReadingInCM, AverageDistanceReadingInCM));
        }

        public double GetDistanceOfObjectFromSensor()
        {
            // get total distance pulse travelled and divide by two since
            // the pulse traveled to the object and back
            double CurrentRecentDistanceReadingInCM = GetDistanceThatThePulseTravelledInCentimeters() / 2.0;
            if(isValidDistanceReading(CurrentRecentDistanceReadingInCM))
            {
                MostRecentDistanceReadingInCM = CurrentRecentDistanceReadingInCM;
            }

            return MostRecentDistanceReadingInCM;
        }

        private bool isValidDistanceReading(double currentRecentDistanceReadingInCM)
        {
            if(currentRecentDistanceReadingInCM < ULTRASONIC_RANGE_FINDER_MIN_RANGE_CM)
            {
                return false;
            }

            if(currentRecentDistanceReadingInCM > ULTRASONIC_RANGE_FINDER_MAX_RANGE_CM)
            {
                return false;
            }

            return true;
        }

        public double GetDistanceThatThePulseTravelledInCentimetersORIG()
        {
            ManualResetEvent mre = new ManualResetEvent(false);

            // give time for any current pulses to reflect and clear
            mre.WaitOne(TimeSpan.FromMilliseconds(MaxMillisecondsForRoundTrip()));


            stopwatch.Reset();


            //Send pulse
            this.TriggerPin.Write(GpioPinValue.High);
            mre.WaitOne(TimeSpan.FromMilliseconds(ULTRASONIC_RANGE_MILLISECONDS_DURATION_OF_PULSE_TO_SEND));
            this.TriggerPin.Write(GpioPinValue.Low);

            //// now that we have finished the pulse, start the stopwatch to time the duration to the end of the echo
            //stopwatch.Start();

            // wait for start of echo
            while (this.EchoPin.Read() != GpioPinValue.High)
            {
            }

            // now that we got the start of the echo, start our stopwatch
            stopwatch.Start();


            // wait for end of echo
            while (this.EchoPin.Read() == GpioPinValue.High)
            {
            }
            stopwatch.Stop();


            //Calculating distance
            double distance = stopwatch.Elapsed.TotalSeconds * CENTIMETERS_PER_SECOND;


            return distance;
        }

        private double GetDistanceThatThePulseTravelledInCentimeters()
        {
            if(stopwatch == null)
            {
                return -33.4567d;
            }
            var t = Task.Run(() =>
            {
                ManualResetEvent mre = new ManualResetEvent(false);

                // give time for any current pulses to reflect and clear
                mre.WaitOne(TimeSpan.FromMilliseconds(MaxMillisecondsForRoundTrip()));


                stopwatch.Reset();


                //Send pulse
                this.TriggerPin.Write(GpioPinValue.High);
                mre.WaitOne(TimeSpan.FromMilliseconds(ULTRASONIC_RANGE_MILLISECONDS_DURATION_OF_PULSE_TO_SEND));
                this.TriggerPin.Write(GpioPinValue.Low);

                //// now that we have finished the pulse, start the stopwatch to time the duration to the end of the echo
                //stopwatch.Start();

                // wait for start of echo
                while (this.EchoPin.Read() != GpioPinValue.High)
                {
                }

                // now that we got the start of the echo, start our stopwatch
                stopwatch.Start();


                // wait for end of echo
                while (this.EchoPin.Read() == GpioPinValue.High)
                {
                }
                stopwatch.Stop();


                //Calculating distance
                double distance = stopwatch.Elapsed.TotalSeconds * CENTIMETERS_PER_SECOND;


                return distance;
            });


            bool didComplete = t.Wait(TimeSpan.FromMilliseconds(200));

            if (didComplete)
            {
                return t.Result;
            }
            else
            {
                return 4 * -3.14;
            }
        }

        public double GetDistance()
        {
            ManualResetEvent mre = new ManualResetEvent(false);
            mre.WaitOne(500);
            stopwatch.Reset();
            //Send pulse
            this.TriggerPin.Write(GpioPinValue.High);
            mre.WaitOne(TimeSpan.FromMilliseconds(0.01));
            this.TriggerPin.Write(GpioPinValue.Low);
            return this.PulseIn(EchoPin, GpioPinValue.High);
        }

        private double PulseIn(GpioPin echoPin, GpioPinValue value)
        {
            var t = Task.Run(() =>
            {
                //Recieve pusle
                while (this.EchoPin.Read() != value)
                {
                }
                stopwatch.Start();

                while (this.EchoPin.Read() == value)
                {
                }
                stopwatch.Stop();
                //Calculating distance
                double distance = stopwatch.Elapsed.TotalSeconds * 17000;
                return distance;
            });


            bool didComplete = t.Wait(TimeSpan.FromMilliseconds(200));
            //bool didComplete = t.Wait(TimeSpan.FromMilliseconds(700));

            if (didComplete)
            {
                return t.Result;
            }
            else
            {
                return 2 * -3.14;
            }
        }

    }
}
