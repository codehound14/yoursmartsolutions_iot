﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Gpio;

namespace YourSmartSolutions.IoT.Devices.Sensors.UltrasonicRangeSensors.HC_SR04
{
    public class HC_SR04 : UltrasonicRangeSensors.UltrasonicRangeSensor
    {
        // set the device specific ID
        new public string DevicePartNumberIdentifier { get { return "HC-SR04"; } }

        public HC_SR04(UltrasonicRangeSensorPinConnections ultrasonicRangeSensorPinConnections) : base(GpioController.GetDefault(), ultrasonicRangeSensorPinConnections.Trigger, ultrasonicRangeSensorPinConnections.Echo)
        {
            this.ContinuousUpdateFrequencyMilliseconds = 2000;
            this.SendSensorDataContinuallyBasedOntimer = true;
        }
    }
}
