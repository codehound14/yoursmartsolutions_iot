﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YourSmartSolutions.IoT.Devices;
using YourSmartSolutions.IoT.Devices.Sensors;

namespace YSS.IoT.Devices.Sensors.Weather
{
    public class Weather : YourSmartSolutions.IoT.Devices.Sensors.SensorBase
    {
        internal object GetReadingFromSensorAndRaiseEventWithResults()
        {
            string test = DeviceInstanceNickname;
            System.Diagnostics.Debug.WriteLine($"In Weather, Asked to get reading for {DeviceInstanceNickname}");
            return -88;
        }
    }
}
